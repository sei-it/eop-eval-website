$(document).ready(function() {
	
	
// first clear the cookies

	//removeCookies();
	
	
// Check navigation cookie - add class if cookie exists
	
	function checkNavCookie() {
	if (document.cookie.indexOf("Welcome") >= 0) {
  		$("span#school-Welcome").addClass("visitedNav");
		$("span#district-Welcome").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Eval1") >= 0){
		$("span#school-Step1").addClass("visitedNav");
		$("span#district").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Eval2") >= 0){
		$("span#school-Step2").addClass("visitedNav");
		$("span#district-emergency-planning").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Eval3") >= 0){
		$("span#school-Step3").addClass("visitedNav");
		$("span#district-wide-emergencies").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Eval4") >= 0){
		$("span#school-Step4").addClass("visitedNav");
		$("span#district-ind-school-plans").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Eval5") >= 0){
		$("span#school-Step5").addClass("visitedNav");
		$("span#district-Conclusion").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Eval6") >= 0){
		$("span#school-Step5").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Eval7") >= 0){
		$("span#school-Conclusion").addClass("visitedNav");
	}		
}

// Add the selected nav class to current page

// SCHOOL NAVIGATION
		
    if ($("span#pageNo").html() == "Page 1 of 8") {
        $("span#school-Welcome").addClass("selectedNav");
		
    } else {
        $("span#school-Welcome").removeClass("selectedNav");	
    }
    if ($("span#pageNo").html() == "Page 2 of 8") {
        $("span#school-Step1").addClass("selectedNav");
		Cookies.set('visited1', 'Welcome');
	
    } else {
        $("span#school-Step1").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageNo").html() == "Page 3 of 8") {
        $("span#school-Step2").addClass("selectedNav");
		Cookies.set('visited2', 'Eval1');
	
    } else {
        $("span#school-Step2").removeClass("selectedNav");
		checkNavCookie(); 
    }

    if ($("span#pageNo").html() == "Page 4 of 8") {
        $("span#school-Step3").addClass("selectedNav");
		Cookies.set('visited3', 'Eval2');
	
    } else {
        $("span#school-Step3").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageNo").html() == "Page 5 of 8") {
        $("span#school-Step4").addClass("selectedNav");
		Cookies.set('visited4', 'Eval3');
	
    } else {
        $("span#school-Step4").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageNo").html() == "Page 6 of 8") {
        $("span#school-Step5").addClass("selectedNav");
		Cookies.set('visited5', 'Eval4');

    } else {
        $("span#school-Step5").removeClass("selectedNav");
		checkNavCookie();
    }

    if ($("span#pageNo").html() == "Page 7 of 8") {
        $("span#school-Step6").addClass("selectedNav");
		Cookies.set('visited6', 'Eval5');
	
    } else {
        $("span#school-Step6").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageNo").html() == "Page 8 of 8") {
        $("span#school-Conclusion").addClass("selectedNav");
		Cookies.set('visited7', 'Eval7');
	
    } else {
        $("span#school-Conclusion").removeClass("selectedNav");
		checkNavCookie();	
    }


// DISTRICT NAVIGATION
		
    if ($("span#pageNo").html() == "Page 1 of 6") {
        $("span#district-Welcome").addClass("selectedNav");
		
    } else {
        $("span#district-Welcome").removeClass("selectedNav");	
    }
    if ($("span#pageNo").html() == "Page 2 of 6") {
        $("span#district").addClass("selectedNav");
		Cookies.set('visited1', 'Welcome');
	
    } else {
        $("span#district").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageNo").html() == "Page 3 of 6") {
        $("span#district-emergency-planning").addClass("selectedNav");
		Cookies.set('visited2', 'Eval1');
	
    } else {
        $("span#district-emergency-planning").removeClass("selectedNav");
		checkNavCookie(); 
    }

    if ($("span#pageNo").html() == "Page 4 of 6") {
        $("span#district-wide-emergencies").addClass("selectedNav");
		Cookies.set('visited3', 'Eval2');
	
    } else {
        $("span#district-wide-emergencies").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageNo").html() == "Page 5 of 6") {
        $("span#district-ind-school-plans").addClass("selectedNav");
		Cookies.set('visited4', 'Eval3');
	
    } else {
        $("span#district-ind-school-plans").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageNo").html() == "Page 6 of 6") {
        $("span#district-Conclusion").addClass("selectedNav");
		Cookies.set('visited5', 'Eval4');

    } else {
        $("span#district-Conclusion").removeClass("selectedNav");
		checkNavCookie();
    }


	
// Delete the navigation cookies when returning to main page


	$( ".link-container a" ).on( "click", function() {
		removeCookies();
	});
	
	
	function removeCookies() {
			Cookies.remove('visited1');
			Cookies.remove('visited2');	
			Cookies.remove('visited3');
			Cookies.remove('visited4');
			Cookies.remove('visited5');	
			Cookies.remove('visited6');
			Cookies.remove('visited7');
			Cookies.remove('visited8');	
			Cookies.remove('visited9');	
	}	

});