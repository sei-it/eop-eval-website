// JavaScript Document

jQuery(document).ready(function ($) {

	$('#checkbox').ready(function(){
		setInterval(function () {
			moveRight();
		}, 8000);
	  });
  
  	var slideCount = $('#newsRotator ul li').length;
	var slideWidth = $('#newsRotator ul li').width();
	var slideHeight = $('#newsRotator ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	$('#newsRotator').css({ width: slideWidth, height: slideHeight });
	
	$('#newsRotator ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
    $('#newsRotator ul li:last-child').prependTo('#newsRotator ul');

    function moveLeft() {
        $('#newsRotator ul').animate({
            left: + slideWidth
        }, 600, function () {
            $('#newsRotator ul li:last-child').prependTo('#newsRotator ul');
            $('#newsRotator ul').css('left', '');
        });
    };

    function moveRight() {
        $('#newsRotator ul').animate({
            left: - slideWidth
        }, 900, function () {
            $('#newsRotator ul li:first-child').appendTo('#newsRotator ul');
            $('#newsRotator ul').css('left', '');
        });
    };

    $('a.nrtcontrol_prev').click(function () {
        moveLeft();
    });

    $('a.nrtcontrol_next').click(function () {
        moveRight();
    });
}); 
