﻿<%@ Page Title="" Language="C#" MasterPageFile="EOPEvals.master" AutoEventWireup="true" CodeFile="k12IHEevalTool.aspx.cs" Inherits="k12IHEevalTool" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/mpe.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    function UnloadConfirm() {
        // var tmp = $('input[name=ctl00$ContentMain$hfSaved]').val();
        //  if (tmp == "0")
        //      return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
    }
    $(function () {
        $(".postbutton").click(function () {
            //    $('input[name=ctl00$ContentMain$hfSaved]').val('1');
        });
        $(".change").change(function () {
            //    $('input[name=ctl00$ContentMain$hfSaved]').val('0');
        });
        //window.onbeforeunload = UnloadConfirm;
    });

    $(document).ready(function () {
        $(".MouesoverPopup").click(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 2 //the time to fade-in
            })
        });

        $("#<%=lbtnk12PrintReport.ClientID %>, #<%=lbtnIHEPrintReport.ClientID %>").click(function () {
            blockUIForDownload();
        });
    });

    var fileDownloadCheckTimer;
    function blockUIForDownload() {
        var token = new Date().getTime(); //use the current timestamp as the token value
        $('#<%=hfTokenID.ClientID %>').val(token);
        $.blockUI({ message: '<img src="Images/evaltool/loadingAnimation.gif" />' });
        fileDownloadCheckTimer = window.setInterval(function () {
            var cookieValue = $.cookie('fileDownloadToken');
            var maxtimeout = token + 8000;
            var currenttime = new Date().getTime();

            if (cookieValue == token || (currenttime > maxtimeout))
                finishDownload();
        }, 1000);
    }

    function finishDownload() {
        window.clearInterval(fileDownloadCheckTimer);
        $.cookie('fileDownloadToken', null); //clears this cookie value
        $.unblockUI();
    }


    function resetRadionbuttons() {
        // $('input[type=radio]').prop('checked', false);
        $('input[type=radio]').removeAttr("checked");
    }
    </script>
<style type="text/css">
        a:hover 
        {
            cursor:pointer;
        }
        
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
        }
        .msapDataTbl th
        {
            color: #000;
        }
		.formLayout p table
		{
			margin-left: 320px;
			border: thin solid #000000;
		}
        tablens900 table{
            
            border-style: solid;
            border-width: thin;
        }
        tablens900 tr,tablens900 td {
           
            border: thin solid #000000;
        }
        
        div.testtable table
        {
            border-style: solid;
            border-width: thin;
        }
        
        div.testtable td { 
    padding: 5px;
	border-left: 0px dotted #ccc;
        }

        div.testtable input[type="radio"]
        {
           margin-left: 20px;
           margin-right: 3px;
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheader" Runat="Server">
        <!--BEGIN NAVIGATION STEPS-->
        <div class="eval-navigation">
            	<div class="steps" id="school">
                	<ul>
                    <li class="stepNav"><span id="Welcome" runat="server">Welcome</span></li>
                    <li class="stepNav"><span id="Step1" runat="server">Step 1</span></li>
                    <li class="stepNav"><span id="Step2" runat="server">Step 2</span></li>
                    <li class="stepNav"><span id="Step3" runat="server">Step 3</span></li>
                    <li class="stepNav"><span id="Step4" runat="server">Step 4</span></li>
                    <li class="stepNav"><span id="Step5" runat="server">Step 5</span></li>
                    <li class="stepNav"><span id="Step6" runat="server">Step 6</span></li>
                    <li class="stepNav"><span id="Conclusion" runat="server">Conclusion</span></li>
                    </ul>
                </div>
        </div>
        <!--END NAVIGATION STEPS-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Contentmain" Runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="container"> 
    <div class="eval-content">
    

           
<table width="100%">
<tr>
<td style="text-align:left">
 <asp:DropDownList ID="ddlShcoolType" runat="server" AutoPostBack="true" onchange="resetRadionbuttons()" CausesValidation="false"
               onselectedindexchanged="ddlShcoolType_SelectedIndexChanged" Visible="false">
           <asp:ListItem>K-12</asp:ListItem>
           <asp:ListItem>IHE</asp:ListItem>
           <asp:ListItem>District</asp:ListItem>
           </asp:DropDownList>
</td>

<td>
</td>
</tr>
</table>

<asp:HiddenField ID="hfSaved" runat="server" Value="1" />

    <asp:Panel ID="pnlEvallist" runat="server" Visible="false">
<div>
    <asp:Button ID="btnNew" runat="server"  Text="new evaluation" 
        onclick="btnNew_Click" />
</div>
<div style="margin-bottom: 0px">
    <asp:GridView ID="gvlist" runat="server" EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True"
          AutoGenerateColumns="False" DataKeyNames="ID" CellPadding="4" 
        ForeColor="#333333" GridLines="None" DataSourceID="SqlDataSource1" >
          <AlternatingRowStyle BackColor="White" />
          <Columns>
            <asp:BoundField DataField="schtype" SortExpression="" HeaderText="Affiliate Type" />
            <asp:BoundField DataField="orgtype"  SortExpression="" HeaderText="Organization Type" />
            <asp:BoundField DataField="orgrole" SortExpression="" HeaderText="Organization Role" />
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnNonegEdit" runat="server" Text="Edit" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                   <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
          <EditRowStyle BackColor="#2461BF" />
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle Wrap="False" BackColor="#507CD1" Font-Bold="True" 
              ForeColor="White" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <RowStyle BackColor="#EFF3FB" />
          <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
          <SortedAscendingCellStyle BackColor="#F5F7FB" />
          <SortedAscendingHeaderStyle BackColor="#6D95E1" />
          <SortedDescendingCellStyle BackColor="#E9EBEF" />
          <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TAEvlConnectionString %>" 
        SelectCommand="SELECT * FROM [k12evaltool] WHERE ([schtype] = @schtype AND isDeleted=0)">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlShcoolType" Name="schtype" 
                PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</div>
</asp:Panel>
    
    <asp:MultiView ID="mvk12" runat="server" ActiveViewIndex="0" >
        <asp:View ID="View1" runat="server" >
        <h1>For K-12 Schools</h1>
                <p>
                Welcome to EOP EVALUATE! This tool is designed to help you evaluate an existing school emergency operations plan (EOP) against the guidelines set forth
                in the Federal <i>Guide for Developing High-Quality School Emergency Operations Plans (School Guide)</i>. EOP EVALUATE for K-12 Schools is set up to help you 
                determine the extent to which your existing plan aligns with the recommendations made by the U.S. Departments of Education, Homeland Security with the Federal Emergency Management Agency, Justice with the Federal Bureau of Investigation,
                and Health and Human Services, specifically, the planning principles, six-step planning process for plan development, and the structure of your plan,
                 as set forth in the Federal guidance. The process of completing this tool will take you through the Federal guidance itself, and reading and responding
                 to the prompts provided will help illustrate the concepts and recommendations provided in the <i>School Guide</i>. You may use this tool to help identify areas 
                for plan enhancement, recognize strengths in your existing plan, and/or to learn more about the recommendations set forth in the Federal guidance. 
                Please note that completion of this tool requires some familiarity with the <i>School Guide</i> and the terminology used therein, and assumes the user possesses 
                extensive knowledge of the content of their school or district EOP. For more information on the Federal guidance, please visit the Readiness and 
                Emergency Management for Schools (REMS) Technical Assistance (TA) Center’s Website at 
                 <a href='http://rems.ed.gov/K12GuideForDevelHQSchool.aspx' target='_blank'>http://rems.ed.gov/K12GuideForDevelHQSchool.aspx</a>.
                <telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip0" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid0" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
                </p>
                <p>
                   Upon completing EOP EVALUATE for K-12 Schools, your responses will result in a customized downloadable report containing a variety of additional resources and information relevant to your needs. This tool should take approximately 18-25 minutes to complete. Please note that no information is recorded during this process; your responses are used only to generate your customized report, and no data remains in our system upon completion and download of the report.
                </p>
                <p>
                  For assistance using this tool, please contact the REMS TA Center Help Desk by email (<a href="mailto:info@remstacenter.org">info@remstacenter.org</a>) or by telephone (855-781-7367 [REMS]) toll-free Monday through Friday between the hours of 9:00 a.m. and 5:00 p.m., Eastern Time.
                </p>
                <p>
                Let’s get started! Please note that all fields are required. Click NEXT to continue.
                   </p>



              <h1>Demographic Information</h1>              
                <p>
                  <span>What type of organization are you from? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </span>
                    <asp:DropDownList ID="ddlk12orgtype" runat="server">
                    <asp:ListItem >Select One</asp:ListItem>
	                <asp:ListItem>LEA</asp:ListItem>
	                <asp:ListItem>SEA</asp:ListItem>
	                <asp:ListItem>K-12 School</asp:ListItem>
	                <asp:ListItem>Public School</asp:ListItem>
	                <asp:ListItem>Non-public School</asp:ListItem>
	                <asp:ListItem>High School</asp:ListItem>
	                <asp:ListItem>Middle School</asp:ListItem>
	                <asp:ListItem>Elementary School</asp:ListItem>
	                <asp:ListItem>Magnet School</asp:ListItem>
	                <asp:ListItem>Other</asp:ListItem>
                    </asp:DropDownList>
                   
                   <asp:RequiredFieldValidator   
        ID="RequiredFieldValidator127"  
        runat="server"  
        ControlToValidate="ddlk12orgtype" ForeColor="Red" SetFocusOnError="true" ValidationGroup="vgView2"
        ErrorMessage="*Required"  InitialValue="Select One"  />
                 </p>
                 <p>
              <span>What is your role within your organization?
                 &nbsp;&nbsp;&nbsp;
               </span>
                 <asp:DropDownList ID="ddlk12orgrole" runat="server">
                    <asp:ListItem >Select One</asp:ListItem>
                    <asp:ListItem>School Emergency Management</asp:ListItem>
                    <asp:ListItem>Law Enforcement Officer</asp:ListItem>
                    <asp:ListItem>School Resource Officer</asp:ListItem>
                    <asp:ListItem>Principal</asp:ListItem>
                    <asp:ListItem>Teacher</asp:ListItem>
                    <asp:ListItem>Administrator</asp:ListItem>
                    <asp:ListItem>Business Office</asp:ListItem>
                    <asp:ListItem>Central Administration or Designee</asp:ListItem>
                    <asp:ListItem>Support Personnel (Instructional Aide)</asp:ListItem>
                    <asp:ListItem>Counselor</asp:ListItem>
                    <asp:ListItem>Social Worker</asp:ListItem>
                    <asp:ListItem>Psychologist</asp:ListItem>
                    <asp:ListItem>Nurse</asp:ListItem>
                    <asp:ListItem>Maintenance Staff</asp:ListItem>
                    <asp:ListItem>Food Service or Cafeteria Worker</asp:ListItem>
                    <asp:ListItem>Bus Driver</asp:ListItem>
                    <asp:ListItem>Environmental Health and Safety</asp:ListItem>
                    <asp:ListItem>Facilities and Operations</asp:ListItem>
                    <asp:ListItem>Human Resources</asp:ListItem>
                    <asp:ListItem>Information Technology</asp:ListItem>
                    <asp:ListItem>Public Information Officer</asp:ListItem>
                    <asp:ListItem>Other</asp:ListItem>
                 </asp:DropDownList>

                 <asp:RequiredFieldValidator   
        ID="RequiredFieldValidator126"  
        runat="server"  
        ControlToValidate="ddlk12orgrole" ForeColor="Red" SetFocusOnError="true" ValidationGroup="vgView2"
        ErrorMessage="*Required"  InitialValue="Select One"  />
                 </p>
                <p>&nbsp;
                    </p>

                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button1" runat="server" Text="Previous" Visible="false" Enabled="false" ForeColor="Gray" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button2" runat="server" Text="Next" ValidationGroup="vgView2" OnClick ="K12NextClick" />
            </div>
        </div>
        </asp:View>
 <%-- <asp:View ID="View2" runat="server">--%>
    
                <!-- BEGIN PREV-NEXT NAVIGATION -->
   <%-- <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button3" runat="server" Text="Previous"  OnClick ="K12PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button4" runat="server" Text="Next" ValidationGroup="vgView2" OnClick ="K12NextClick" />
            </div>
        </div>--%>
<%-- </asp:View>--%>

        <asp:View ID="View3" runat="server">
        <div class="intro-note">
            <p>
                        EOP EVALUATE will help you determine the extent to which your existing EOP aligns 
                         with the recommendations set forth in the Federal <i>Guide for Developing High-Quality School 
                         Emergency Operations Plans (School Guide).</i> Use knowledge of the content of 
                         your EOP, and the process your school or district underwent to create your EOP, 
                         to respond to the following questions. 
            </p>
        </div>
        <h2>Step 1: A high-quality plan is supported by a collaborative process that includes multiple perspectives.</h2>
        	<div class="question">
                
                1. Does your school or district have a <a id="popupid1" title=" For more information on forming a collaborative planning team, see pp. 5-7 in the <i>School Guide</i> or visit <a href='http://rems.ed.gov/K12PPStep01.aspx' target='_blank'>http://rems.ed.gov/K12PPStep01.aspx</a>" >collaborative planning team</a> that includes 
                representation (i.e., in-person participation OR input by way of consultation OR 
                through incorporation of expertise and resources) from the following entities? 
                Please note, rural sites or smaller schools may have single individuals 
                representing multiple stakeholder roles. Indicate below which roles are 
                represented on your collaborative planning team.
                
            </div>
            
            <telerik:RadToolTip Skin="Sunset" runat="server" ID="toolTipTotalHigher" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid1" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<div class = "testtable">
   <section>
    <div class="topic">SCHOOL COMMUNITY</div>
        <fieldset>
            <div class="questionBlock">
                	<div class="col-lg-6 col-xs-12">School Leadership</div>
                    <div class="col-lg-4 col-xs-12">
                        <asp:RadioButtonList
                        ID="rblstk12isschcomleaderAffair"
                        runat="server"
                        RepeatDirection="Horizontal"
                        RepeatLayout="Flow">
                            <asp:ListItem>Yes</asp:ListItem>
                            <asp:ListItem>No</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>                
                    <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="ReqiredFieldValidator1"  
                        runat="server"  
                        ControlToValidate="rblstk12isschcomleaderAffair"
                        ValidationGroup="vgView3"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                </div>
            </div>
        </fieldset>
        
        <fieldset>
        	<div class="questionBlock">
            	<div class="col-lg-6 col-xs-12">School Administrators</div>
                <div class="col-lg-4 col-xs-12">
        			<asp:RadioButtonList
                    ID="rblstk12isschcomadminOffice"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
            			<asp:ListItem>Yes</asp:ListItem>
            			<asp:ListItem>No</asp:ListItem>
            		</asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                    runat="server"
                    ValidationGroup="vgView3"
                    ControlToValidate="rblstk12isschcomadminOffice"
                    ForeColor="Red"
                    SetFocusOnError="true"
                    ErrorMessage="*Required" />                
                </div>
            </div>
        </fieldset>
        
        <fieldset>
        <div class="questionBlock">
            <div class="col-lg-6 col-xs-12">Educators</div>
            <div class="col-lg-4 col-xs-12">
                <asp:RadioButtonList
                ID="rblstk12isschcomeduDesignee"
                runat="server"
                RepeatDirection="Horizontal"
                RepeatLayout="Flow">
                    <asp:ListItem>Yes</asp:ListItem>
                    <asp:ListItem>No</asp:ListItem>
            	</asp:RadioButtonList>                
            </div>                
            <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                    runat="server"
                    ValidationGroup="vgView3"
                    ControlToValidate="rblstk12isschcomeduDesignee"
                    ForeColor="Red" SetFocusOnError="true"
        			ErrorMessage="*Required" />
             </div>
            </div>
        </fieldset>        
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">School District</div>                					<div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomDistrictService"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomDistrictService"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset>          

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">School Psychologists</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcompsychSafety"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcompsychSafety"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset>          

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">School Counselors</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomconslorOperation"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomconslorOperation"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset>  

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">School Nurses</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomnurseService"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator11"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomnurseService"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
         
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Facilities Managers</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomfmanagerHservice"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator9"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomfmanagerHservice"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Transportation Managers</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomtmanagerHRes"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator8"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomtmanagerHRes"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Food Personnel</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomfpersonITech"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomfpersonITech"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
          <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Custodial Staff</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomcstaffCousel"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomcstaffCousel"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
          <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">School Resource Officers (SROs) and school-based safety and security officers</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomSROsOffice"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator10"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomSROsOffice"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
          <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">IT Specialists</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomITSpecOperation"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator12"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomITSpecOperation"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
          <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Family Services Representatives</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomfsrepLife"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator13"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomfsrepLife"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

          <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Students</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomstudentAffair"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator14"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomstudentAffair"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset>
         
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Families</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isschcomfamilyTrans"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator15"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isschcomfamilyTrans"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Representatives From Area Schools</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12ischcomASchrepOffice"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator16"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12ischcomASchrepOffice"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset>   
    </section>

	<section>
    <div class="topic">INTERESTS REPRESENTED</div>
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Individuals with Disabilities and Others With Access and Functional Needs</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isINTREPindiv"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator17"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isINTREPindiv"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
    </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Racial and Ethnic Minorities</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isINTREPracial"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator18"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isINTREPracial"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Religious</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isINTREPReligious"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator19"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isINTREPReligious"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Limited English Proficiency</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isINTREPlimited"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator20"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isINTREPlimited"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
    </section>
	
    <section>
    	<div class="topic">COMMUNITY PARTNERS</div>
          <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">First Responders</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isComPart1stResp"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator37"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isComPart1stResp"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Local Emergency Management Staff</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isComPartlocalStaff"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator21"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isComPartlocalStaff"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
 
          <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Law Enforcement Officers</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isComPartLawofficer"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator22"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isComPartLawofficer"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Emergency Medical Services Personnel</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isComPartEMSPersonnel"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator23"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isComPartEMSPersonnel"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
 
       <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">School Resource Officers (SROs)</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isComPartSchResOfficer"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RV1"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isComPartSchResOfficer"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
     
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Fire Officials</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isComPartFireOfficial"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator25"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isComPartFireOfficial"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Public Health Practitioners</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isComPartPHPract"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator26"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isComPartPHPract"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Mental Health Practitioners</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isComPartMHPract"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator27"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isComPartMHPract"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
</section>

<section>
    <div class="topic">ADDITIONAL PARTNERS WITH ROLE IN SCHOOL EMERGENCY MANAGEMENT</div>
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Policy Makers and Local Elected Officials</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isAPRSEMPMLEofficial"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator28"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isAPRSEMPMLEofficial"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Community-based Religious Organizations</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isAPRSEMCBRorganize"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator29"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isAPRSEMCBRorganize"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Business Partners</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isAPRSEMBusPart"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator30"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isAPRSEMBusPart"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Community-based Youth Organizations</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isAPRSEMCBYOrganize"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator31"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isAPRSEMCBYOrganize"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">State and Federal Law Enforcement (FBI, DEA, ATF, etc.) and Homeland Security 
               Officials</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isAPRSEMSFLEHSOfficial"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator32"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isAPRSEMSFLEHSOfficial"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">District and State Attorney General</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isAPRSEMDSAGeneral"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator33"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isAPRSEMDSAGeneral"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Local Social Services Departments</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isAPRSEMLSSDepart"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator34"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isAPRSEMLSSDepart"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Disaster Organizations (e.g., American Red Cross)</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isAPRSEMDisOrganize"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator35"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isAPRSEMDisOrganize"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Media</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12isAPRSEMMedia"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator36"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12isAPRSEMMedia"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
	</section>
</div>
          
<div class="question last-section">
           2.	When starting out, did your collaborative planning team form a shared approach to developing the EOP, including each of the following:
           </div>
           
<div class = "testtable">
   
   <section>
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">a. Developing a common framework in which all parties understand each other’s vocabulary, command structure, culture, and use of common critical concepts and principles, such as the National Incident Management System (NIMS). </div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is2NIMS"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator38"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12is2NIMS"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">b. Establishing each member’s unique role and responsibilities.</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is2UniRole"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator39"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12is2UniRole"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">c. Determining a regular schedule of meetings.</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is2RegMeeting"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator40"
                        runat="server"
                        ValidationGroup="vgView3"
                        ControlToValidate="rblstk12is2RegMeeting"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
   </section>
</div>
        
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button5" runat="server" Text="Previous"  ValidationGroup="vgView3" CausesValidation="false" OnClick ="K12PrevClick" />&nbsp;&nbsp;
        <asp:Button ID="Button6" runat="server" Text="Next" ValidationGroup="vgView3"  OnClick ="K12NextClick" />
            </div>
        </div>
        </asp:View>
<asp:View ID="View4" runat="server">
        
        <h2>Step 2: A high-quality plan is customized based on a data-informed understanding of the situation.</h2>
        
<section>        
<div class="question">
3. Did your planning team conduct assessments of your school site to better understand threats and hazards that your school may face?
  Examples of recommended assessments include:	

</div>

<div class="testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-4 col-xs-12">
<ul>
<li><a id="popupid3" title="A site assessment examines the safety, accessibility, and emergency preparedness of the school’s buildings and grounds (e.g., a review of building access/egress control measures, visibility around building’s exterior, structural integrity, compliance with standards for individuals with disabilities and others with access and functional needs, emergency vehicle access, etc.).">Site Assessments</a> 
</li>
<li><a id="popupid4" title="A culture and climate assessment evaluates student and staff connectedness to the school, and the presence of problem behaviors. This can reveal students’ and staff’s perceptions of their safety, and create awareness of problem behaviors that need to be addressed to improve school climate. A positive school climate is one in which students are more likely to succeed, feel safe, and report threats.">Culture and Climate Assessments</a> 
</li>
<li><a id="popupid5"  title="A school threat assessment analyzes communication and behaviors to determine whether or not a student, staff, or other person may pose a threat. These assessments must be based on fact, must comply with applicable laws, and are often conducted by multidisciplinary threat assessment teams.">School Behavioral Threat Assessments</a>
</li>
<li><a id="popupid6" title=" A capacity assessment examines the capabilities of students and staff as well as the services and material resources of community partners. This is used to identify people in the building with applicable skills (e.g., first aid certification) and inventory equipment and supplies (e.g., evacuation chairs for individuals with disabilities and others with access and functional needs).">Capacity Assessments</a>
</li>
</ul>
</div>
<div class="col-lg-6 col-xs-12">
<asp:RadioButtonList ID="rblstk12nm5Highquality" runat="server"  RepeatLayout="Flow" >
<asp:ListItem>Yes, multiple assessments were conducted</asp:ListItem>
<asp:ListItem>Yes, two assessments were conducted</asp:ListItem>
<asp:ListItem>No, only one assessment was completed</asp:ListItem> 
<asp:ListItem>No, no assessments at all were completed</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div> 
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator169" runat="server"  ControlToValidate="rblstk12nm5Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView4" />
</div>
</div>
  
</fieldset>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip62" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid3" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip63" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid4" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip64" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid5" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip65" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid6" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
</div>             

</section>

        <%--<div class="intro-note">
        Next, thinking about the list of threats and hazards identified, did your planning team assess the risk posed by the identified threats and hazards? Evaluating risk entails understanding the probability that the specific threat or hazard will occur; the effects it will likely have, including the severity of impact; the time the school will have to warn students and staff about the threat or hazard; and how long it may last. Data must be collected to help assess the risk posed. 
</div>--%>

<section>
<div class="question">

4. Did your planning team identify a comprehensive list of both current and 
historical <a id="popupid2" title="For a list of potential threats and hazards schools may face, depending on their unique locale, see p. 36 in the <i>School Guide</i> or visit <a href='http://rems.ed.gov/K12ThreatAndHSAnnex.aspx' target='_blank'>http://rems.ed.gov/K12ThreatAndHSAnnex.aspx.</a> " >threats and hazards</a> in the school and the surrounding community? A 
list is comprehensive when it is informed by data from the following sources: 
    </div>

<div class="testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<ul>
<li>Assessment data from site assessments, culture and climate assessments, school threat assessments, capacity assessments, and other types of assessments   </li>
<li>Team members’ own knowledge, expertise, and existing data collection sources</li>
<li>Local agencies, including, but not limited to:
<ul>
<li>Emergency Management Offices</li>
<li>Fire Department </li>
<li>Police Department</li>
<li>Local Organizations and Community Groups (e.g., local chapter of the American Red Cross, Community Emergency Response Team)</li>
<li>Utility Companies</li>
<li>Businesses</li>
</ul>
</li>
<li>State Agencies</li>
<li>Federal Agencies</li>
</ul>
</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstk12nm3Highquality" runat="server" RepeatLayout="Flow">
<asp:ListItem>Yes, very comprehensive</asp:ListItem>
<asp:ListItem>Yes, somewhat comprehensive</asp:ListItem>
<asp:ListItem>No, somewhat uncomprehensive</asp:ListItem> 
<asp:ListItem>No, not very comprehensive</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator41" runat="server"  ControlToValidate="rblstk12nm3Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView4" />
</div>
</div>
</fieldset>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip1" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid2" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="400" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
        
</div>      
</section>

    <section>
<div class="question">
5.	Evaluating the risk posed by threats and hazards entails understanding the probability that a specific threat or hazard will occur; the effects it will likely have,
    including the severity of impact; the time the school will have to warn students and staff about a threat or hazard; and how long it may last. Did your planning team 
    evaluate the risks posed by identified threats and hazards?
        </div>
       
<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-10 col-xs-12">
<asp:RadioButtonList ID="rblstk12nm4Highquality" runat="server"  RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator42" runat="server"  ControlToValidate="rblstk12nm4Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView4" />
</div>
</div>
</fieldset> 
</div>
</section>


 <section>
            
<div class="question">
6.	After evaluating the risk posed by the identified threats and hazards, did your planning team compare the risks and vulnerabilities of the identified threats and hazards 
      and<a id="popupid7" title="For an example method of how to prioritize threats and hazards, see pp. 11-12 in the <i>School Guide</i> or visit <a href='http://rems.ed.gov/K12PPStep02.aspx' target='_blank'>http://rems.ed.gov/K12PPStep02.aspx.</a>"> create a prioritized list</a> of threats and hazards to address in your EOP? 
</div>
 <telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip2" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid7" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<div class = "testtable">
	<fieldset>
    <div class="questionBlock">
    <div class="col-lg-10 col-xs-12">
 		<asp:RadioButtonList ID="rblstk12nm6Highquality" runat="server" RepeatLayout="Flow">
                   <asp:ListItem>Yes</asp:ListItem>
                   <asp:ListItem>No</asp:ListItem>
                   <asp:ListItem>I don’t know</asp:ListItem>
               </asp:RadioButtonList>
      </div>       
              <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator44" runat="server"  ControlToValidate="rblstk12nm6Highquality" ForeColor="Red" SetFocusOnError="true"
            ErrorMessage="*Required" ValidationGroup="vgView4" />

		</div>
        </div>
      </fieldset>    
 </div>         
</section>
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button7" runat="server" Text="Previous"  OnClick="K12PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button8" runat="server" Text="Next" OnClick="K12NextClick" ValidationGroup="vgView4" />
            </div>
        </div>
        </asp:View>
        <asp:View ID="View5" runat="server">
        <h2>Step 3: A high-quality plan articulates clear goals and objectives to protect persons and property.</h2>

<section>            

<div class="question">
7.	Once your planning team identified a prioritized list of threats and hazards to address in your EOP, they should have developed goals and objectives for addressing each threat or hazard.</div>

<div class="testtable">
<div class="first-section">
<fieldset>
<div class="questionBlock"> 
<div class="col-lg-6 col-xs-12">
a.	Did your planning team develop <a id="popupid9"  title="Goals are broad, general statements that indicate the desired outcome in response to the threat or hazard identified by planners.<br /><br/>Example:<br /><b>Goal 1 (before):</b> Prevent a fire from occurring on school grounds.<br/><b>Goal 2 (during):</b> Protect all persons from injury and property from damage by the fire.<br /><b>Goal 3 (after):</b> Provide necessary medical attention to those in need." ><b>at least three goals</b></a> for addressing each threat or hazard, indicating the desired outcome for 1) before, 2) during, and 3) after the threat or hazard? 


<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip21" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid9" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
</div>   
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstk12nm8aHighquality" runat="server" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12"> 
<asp:RequiredFieldValidator ID="RequiredFieldValidator45" runat="server" ValidationGroup="vgView5" ControlToValidate="rblstk12nm8aHighquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div> 
</div>
</fieldset> 

<fieldset>
      
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
b.	Did your planning team develop <a id="popupid10" title="Objectives are specific, measurable actions that are necessary to achieve the goals. Often, planners will need to identify multiple objectives in support of a single goal.<br /><br/><u>Example:</u><br /><b>Goal 1 (before):</b> Prevent a fire from occurring on school grounds.<br /><b>Objective 1.1:</b> Provide fire prevention training to all students and staff who use combustible materials or equipment.<br /><b>Objective 1.2:</b> Store combustible materials in fireproof containers or rooms."><b>multiple objectives</b></a> (specific, measurable actions that are necessary to achieve the goals) for each goal? 
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip22" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid10" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstk12nm8bHighquality" runat="server" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12"> 
<asp:RequiredFieldValidator ID="RequiredFieldValidator46" runat="server" ValidationGroup="vgView5" ControlToValidate="rblstk12nm8bHighquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>

</div>
</div>   
</section>  

        
<section>           
<div class="question">
8.	Once your team compiled the objectives for the prioritized threats and hazards, did the team identify certain critical  <a id="popupid11" title="Functions are activities that apply to one or more threat or hazard. Examples of these cross-cutting functions include: evacuating; providing medical care; and accounting for all students, staff, and guests."><b>functions</b></a> that apply to more than one threat or hazard (e.g., evacuating; providing medical care; accounting for all students, staff, and guests)?

</div>
<div class="testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-10 col-xs-12">
<asp:RadioButtonList ID="rblstk12nm9Highquality" runat="server"  RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator47" runat="server" ValidationGroup="vgView5" ControlToValidate="rblstk12nm9Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div> 
</div>
</fieldset> 
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip23" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid11" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
</div>          
</section>           
        <div class="intro-note">
           The Federal guidance recommends developing three goals for each function, including the desired outcome for 1) before, 2) during, and 3) after the function has been executed. These commonly occurring functions should have formed the basis for the Functional Annexes component of your school EOP.  
        </div>

                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button9" runat="server" Text="Previous"  OnClick="K12PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button10" runat="server" Text="Next" OnClick="K12NextClick" ValidationGroup="vgView5" />
            </div>
        </div>
        </asp:View>
        <asp:View ID="View6" runat="server">
        <h2>Step 4: A high-quality plan prescribes clear courses of action for accomplishing objectives.</h2>
        <div class="intro-note">
        Courses of action address the what, who, when, where, why, and how for each threat, hazard, and function. 
        </div>
        

<section>
        <div class="question">
       9. Did your planning team use <a id="popupid8" title="What is scenario-based planning? As defined in FEMA’s Community Preparedness Guide (CPG) 101 (2010), &quot;This approach starts with building a scenario for a hazard or a threat. Then, planners analyze the impact of the scenario to determine appropriate courses of action. Planners typically use this planning concept to develop planning assumptions, primarily for hazard- or threat-specific annexes to a basic plan.&quot; <a href='http://www.fema.gov/media-library-data/20130726-1828-25045-0014/cpg_101_comprehensive_preparedness_guide_developing_and_maintaining_emergency_operations_plans_2010.pdf' target='_blank'>http://www.fema.gov/media-library-data/20130726-1828-25045-0014/cpg_101_comprehensive_preparedness_guide_developing_and_maintaining_emergency_operations_plans_2010.pdf</a>"> scenario-based planning</a> to consider your identified threats and hazards? 
        
       
<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-10 col-xs-12">
<asp:RadioButtonList ID="rblstk12nm7Highquality" runat="server" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator125" runat="server" ValidationGroup="vgView5" ControlToValidate="rblstk12nm7Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>  
</div>
</fieldset> 
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip24" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid8" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="800" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
</div>
           
</div>           
 </section>           




<section>
   <div class="question">
     10.	Do the courses of action for each threat, hazard, and function include the following information: 
   </div>
    <div class = "testtable">
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">What action should be taken</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is10taken"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator48"
                        runat="server"
                        ValidationGroup="vgView6"
                        ControlToValidate="rblstk12is10taken"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Who is responsible for the action</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is10responsibleaction"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator49"
                        runat="server"
                        ValidationGroup="vgView6"
                        ControlToValidate="rblstk12is10responsibleaction"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">When the action takes place</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is10takeplace"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator50"
                        runat="server"
                        ValidationGroup="vgView6"
                        ControlToValidate="rblstk12is10takeplace"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">How long does the action take and how much time is actually available</div>                
                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is10available"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator51"
                        runat="server"
                        ValidationGroup="vgView6"
                        ControlToValidate="rblstk12is10available"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset>
         
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">What must happen prior to the action</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is10prioraction"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator52"
                        runat="server"
                        ValidationGroup="vgView6"
                        ControlToValidate="rblstk12is10prioraction"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
    
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">What must happen after the action</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is10afteraction"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator53"
                        runat="server"
                        ValidationGroup="vgView6"
                        ControlToValidate="rblstk12is10afteraction"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset>
         
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Resources necessary for the action</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is10necessaryaction"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator54"
                        runat="server"
                        ValidationGroup="vgView6"
                        ControlToValidate="rblstk12is10necessaryaction"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
         <fieldset>
         
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Consideration for how this action will affect specific populations, such as individuals with disabilities and others with access and functional needs</div>                
                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is10needs"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>                
                <div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator55"
                        runat="server"
                        ValidationGroup="vgView6"
                        ControlToValidate="rblstk12is10needs"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
    </div>
</section>        
<div class="intro-note">
Once your planning team has developed courses of action, they should have compared the costs and benefits of each proposed course of action against the goals and objectives. Based on this comparison, planners should have selected preferred courses of action to move forward in the planning process, and then identified the resources necessary to accomplish each course of action without regard to resource availability. After requirements are identified, they can be matched with existing resources, and any gaps in resources can be identified and addressed.

<p class="first-section">The outcome of Step 4 should have been the creation of your plan’s annexes: </p>
<ul class="first-section">
<li>Goals, objectives, and courses of action for threats and hazards form the Threat- and Hazard-Specific Annexes section of the EOP.</li>
<li>Goals, objectives, and courses of action for functions will be contained in the Functional Annexes section of the EOP.</li>
</ul>         
</div>                
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button11" runat="server" Text="Previous"  OnClick="K12PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button12" runat="server" Text="Next" OnClick="K12NextClick" ValidationGroup="vgView6" />
            </div>
        </div>
        </asp:View>
        <asp:View ID="View7" runat="server">
        
        <h2>Step 5: A high-quality plan is structured, written, reviewed, and shared to communicate clearly the essential components of the school’s approach to emergency management.</h2>
        
<div class="intro-note">
In Step 5, the planning team develops a draft of the school EOP using the courses of action developed in Step 4. The Federal guidance recommends a traditional format that can be tailored to meet individual school needs. This includes three major sections: The Basic Plan, Functional Annexes, and Threat- and Hazard-Specific Annexes.
</div>

<section>

<div class="question">
11.	The <a id="popupid12" title="For more information on the recommended traditional format for an EOP see pp.23-28 in the <i>School Guide</i> or visit <a href='http://rems.ed.gov/K12BasicPlan.aspx' target='_blank'>http://rems.ed.gov/K12BasicPlan.aspx</a>." >Basic Plan</a> section of the school EOP provides an overview of the school’s approach to emergency operations and includes:
</div>


<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-4 col-xs-12">
<ul>
<li><a id="popupid13" title="Introductory material can enhance accountability with community partners. This section typically includes:<ul><li>Cover Page</li><li>Promulgation Documentor Signature Page</li><li>Approval and Implementation Page</li><li>Record of Changes</li><li>Record of Distribution</li><li>Table of Contents </li></ul>" > Introductory Material</a></li>
<li><a id="popupid14" title="The purpose sets the foundation for the rest of the school EOP—it is a general statement of what the EOP is meant to do. The situation section explains why a school EOP is necessary." > Purpose and Situation Overview</a></li>
<li><a id="popupid15" title=" The Concept of Operations section explains the school administrator’s intent with regard to an operation; it is designed to give an overall picture of how the school will protect students, staff, and visitors." > Concept of Operations</a></li>
<li><a id="popupid16" title="This section provides an overview of the broad roles and responsibilities of school staff, families, guardians, and community partners, and of organizational functions during all emergencies." > Organization and Assignment of Responsibilities</a></li>
<li><a id="popupid17" title="This section describes the framework for all direction, control, and coordination activities." > Direction, Control, and Coordination</a></li>
<li><a id="popupid18" title="This section addresses the role of information in the successful implementation of the activities that occur before, during, and after an emergency. " > Information Collection, Analysis, and Dissemination</a></li>
<li><a id="popupid19" title="This section describes the critical training and exercise activities the school will use in support of the plan." > Training and Exercises</a></li>
<li><a id="popupid20" title="This section covers general support requirements and the availability of services and support for all types of emergencies, as well as general policies for managing resources. " > Administration, Finance, and Logistics</a></li>
<li><a id="popupid21" title=" This section discusses the overall approach to planning and the assignment of plan development and maintenance responsibilities." > Plan Development and Maintenance</a></li>
<li><a id="popupid22" title="This section provides the legal basis for emergency operations and activities. " > Authorities and References</a></li>
</ul>

</div>


<div class="col-lg-6 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm11Highquality"
runat="server" 
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>My school EOP includes nearly all of these recommended elements.</asp:ListItem>
<asp:ListItem>My school EOP includes most of these recommended elements.</asp:ListItem>
<asp:ListItem>My school EOP includes some of these recommended elements.</asp:ListItem>
<asp:ListItem>My school EOP includes few of these recommended elements. </asp:ListItem>
</asp:RadioButtonList>         
</div>    
<div class="col-lg-2 col-xs-12">
 <asp:RequiredFieldValidator ID="RequiredFieldValidator56" runat="server"  ControlToValidate="rblstk12nm11Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip7" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid12" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip8" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid13" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip9" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid14" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip10" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid15" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip116" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid16" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip11" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid17" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip12" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid18" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip19" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid19" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip13" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid20" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip14" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid21" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip15" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid22" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip16" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid23" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="350" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
</div>
</section>




<section>
<div class="question">
12.	The <a id="popupid23" title="Functional annexes focus on critical operational functions and the courses of action developed to carry them out. For more information on these 10 functional annexes that schools should address in developing a comprehensive, high-quality EOP see pp. 28-35 in the <i>School Guide</i> or visit <a href='http://rems.ed.gov/K12FuncAnnex.aspx' target='_blank'>http://rems.ed.gov/K12FuncAnnex.aspx.</a> " >Functional Annexes</a> section 
details the goals, objectives, and courses of action of functions (e.g., evacuation, communications, recovery) that apply across multiple threats or hazards. The Federal guidance recommends, at a minimum, the following Functional Annexes be included in your EOP. Indicate whether your EOP contains the following:
</div>

<div class="col-lg-12 col-xs-12 first-section topic">
FUNCTIONAL ANNEXES
</div>

 <div class = "testtable">
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Evacuation</div>
                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is12Evacuation"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>
                <div class="col-lg-2 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator57"
                        runat="server"
                        ValidationGroup="vgView7"
                        ControlToValidate="rblstk12is12Evacuation"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Lockdown</div>                
                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is12Lockdown"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>
                <div class="col-lg-2 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator58"
                        runat="server"
                        ValidationGroup="vgView7"
                        ControlToValidate="rblstk12is12Lockdown"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
    <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Shelter-in-Place</div>
                   <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is12ShelterinPlace"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>
                <div class="col-lg-2 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator59"
                        runat="server"
                        ValidationGroup="vgView7"
                        ControlToValidate="rblstk12is12ShelterinPlace"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

      <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Accounting for All Persons</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is12AccountingAllPersons"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>
                <div class="col-lg-2 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator60"
                        runat="server"
                        ValidationGroup="vgView7"
                        ControlToValidate="rblstk12is12AccountingAllPersons"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
 
        <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Communications and Warning</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is12ComWarning"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>
                <div class="col-lg-2 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator61"
                        runat="server"
                        ValidationGroup="vgView7"
                        ControlToValidate="rblstk12is12ComWarning"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 

         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Family Reunification</div>
                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is12ReunificationAssessment"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>
                <div class="col-lg-2 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator62"
                        runat="server"
                        ValidationGroup="vgView7"
                        ControlToValidate="rblstk12is12ReunificationAssessment"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
 
          <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Continuity of Operations (COOP)</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is12COOP"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>
                <div class="col-lg-2 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator63"
                        runat="server"
                        ValidationGroup="vgView7"
                        ControlToValidate="rblstk12is12COOP"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
          <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Recovery</div>
                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is12Recovery"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>
                <div class="col-lg-2 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator64"
                        runat="server"
                        ValidationGroup="vgView7"
                        ControlToValidate="rblstk12is12Recovery"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
       
    <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Public Health, Medical, and Mental Health</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is12PHMMHealth"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>
                <div class="col-lg-2 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator65"
                        runat="server"
                        ValidationGroup="vgView7"
                        ControlToValidate="rblstk12is12PHMMHealth"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Security</div>
                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is12Security"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>
                <div class="col-lg-2 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator66"
                        runat="server"
                        ValidationGroup="vgView7"
                        ControlToValidate="rblstk12is12Security"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
         <fieldset>
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Do you have additional annexes in your plan that are not on this list?</div>
                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList
                    ID="rblstk12is12onlist"
                    runat="server"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>                
                </div>
                <div class="col-lg-2 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator67"
                        runat="server"
                        ValidationGroup="vgView7"
                        ControlToValidate="rblstk12is12onlist"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        ErrorMessage="*Required" />
                 </div>
                </div>
        </fieldset> 
        
</div>

</section>

<section>

<div class="question">
13. The Threat- and Hazard-Specific Annexes section of the EOP specifies the goals, 
objectives, and courses of action that a school will follow to address a particular type of 
threat or hazard (e.g., hurricane, active shooter situation). While each school’s threat- or
 hazard-specific annexes will vary based on the school planning team’s selection of threats 
 and hazards that are specific to the school, the team should still consider the broader 
 categories listed below. Did your EOP planning consider the following broader categories of 
 threats and hazards?
</div>
<div class="topic first-section">THREAT- OR HAZARD-SPECIFIC ANNEXES</div>
<div class = "testtable">

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<a id="popupid24" title="Examples of Natural Hazards may include:<ul><li>Earthquakes</li><li>Tornadoes</li><li>Lightning</li><li>Hurricanes</li><li>Floods</li><li>Wildfires</li><li>Extreme temperatures</li><li>Landslides or mudslides</li><li>Winter precipitation</li></ul> " >Natural Hazards</a></div> 
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12is13NHazards"
runat="server"
RepeatDirection="Horizontal"
RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator68"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12is13NHazards"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<a id="popupid25" title="Examples of Technological Hazards may include:<ul><li>Explosions or accidental release of toxins from industrial plants</li><li>Dam failure</li><li>Power failure</li><li>Radiological releases from nuclear power stations</li></ul> " >Technological Hazards</a></div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12is13THazards"
runat="server"
RepeatDirection="Horizontal"
RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator69"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12is13THazards"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> 
<a id="popupid26" title="Examples of Biological Hazards may include:<ul><li>Infectious diseases, such as pandemic influenza</li><li>Contaminated food outbreaks, such as E. coli.</li><li>Toxic materials present in school labs</li></ul>" >Biological Hazards</a>  </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12is13BHazards"
runat="server"
RepeatDirection="Horizontal"
RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator70"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12is13BHazards"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> 
<a id="popupid27" title="Examples of Adversarial, Incidental, and Human-caused Threats may include:<ul><li>Fire</li><li>Active shooters</li><li>Criminal threats or actions</li><li>Suicide</li><li>Cyber attacks</li><li>Bomb threats</li></ul>" >Adversarial, Incidental, and Human-caused Threats</a></div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12is13Threats"
runat="server"
RepeatDirection="Horizontal"
RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator71"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12is13Threats"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip25" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid24" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip26" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid25" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip27" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid26" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip28" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid27" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

</div>

</section>


<section>
<div class="question">
14.	When writing the plan, the following guidelines are recommended. Indicate whether these are reflected in your plan. 
</div>

<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> Important information is summarized with checklists and visual aids, such as maps and flowcharts. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm14flowcharts"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Reflected in plan</asp:ListItem>
<asp:ListItem>Not reflected in plan</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator72"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm14flowcharts"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> Language is written clearly, avoiding jargon, minimizing the use of abbreviations, and using short sentences and the active voice. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm14activevoice"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Reflected in plan</asp:ListItem>
<asp:ListItem>Not reflected in plan</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator73"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm14activevoice"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Plan uses a logical, consistent structure that makes it easy for users to grasp the rationale for the sequence of the information and to scan for the information they need. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm14theyneed"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Reflected in plan</asp:ListItem>
<asp:ListItem>Not reflected in plan</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator74"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm14theyneed"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Enough detail is provided to convey an easily understood plan that is actionable.  </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm14actionable"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Reflected in plan</asp:ListItem>
<asp:ListItem>Not reflected in plan</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator75"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm14actionable"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">The plan is presented through accessible tools and documents (i.e., accessible Websites, digital text that can be converted to audio or Braille).</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm14audioBraille"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Reflected in plan</asp:ListItem>
<asp:ListItem>Not reflected in plan</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator76"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm14audioBraille"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 
</div>
</section>

<div class="intro-note">
Once the plan is written, it is important to review it for compliance with applicable laws and for its usefulness in practice.
</div>

<section>

<div class="question">
15.	The following measures can help determine if a plan is of high quality. Indicate the extent to which your plan aligns with each of these criteria.
</div>

<div class = "testtable">
<div class=" col-lg-12 col-xs-12 subquestion"><b>The plan is <i>adequate</i> if it:</b></div>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Identifies and addresses critical courses of action effectively.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15effectively"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator77"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15effectively"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Can accomplish the assigned function.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15assignedfunction"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator78"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15assignedfunction"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> Contains assumptions that are valid and reasonable.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15reasonable"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV2"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15reasonable"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<div class=" col-lg-12 col-xs-12 subquestion"><b>The plan is <i>feasible</i> if:</b></div>       

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">The school can accomplish the assigned function and critical tasks by using available resources within the time contemplated by the plan. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15theplan"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV3"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15theplan"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<div class=" col-lg-12 col-xs-12 subquestion"><b>The plan is <i>acceptable </i> if it:</b></div>  

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Meets the requirements driven by a threat or hazard.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15threathazard"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV4"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15threathazard"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Meets costs and time limitations.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15limitations"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV5"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15limitations"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Is consistent with the law.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15thelaw"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV6"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15thelaw"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<div class=" col-lg-12 col-xs-12 subquestion"><b>The plan is <i>complete </i> if it:</b></div> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Incorporates all courses of action to be accomplished for all selected threats and hazards and identified functions.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15idfunctions"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV7"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15idfunctions"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Integrates the needs of the whole school community.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15schcommunity"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV8"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15schcommunity"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Provides a complete picture of what should happen, when, and at whose direction.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15WhoseDirection"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV9"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15WhoseDirection"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Estimates time for achieving objectives, with safety remaining as the utmost priority.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15utmostpriority"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV10"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15utmostpriority"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Identifies success criteria and a desired end state.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15endstate"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV11"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15endstate"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<div class=" col-lg-12 col-xs-12 subquestion"><b>A plan must <i>comply</i>:</b></div> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">With applicable state and local requirements. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm15localrequirements"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV12"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm15localrequirements"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

</div>

</section>

<section>
<div class="question">
16.	A high-quality plan should also incorporate the following planning principles. Consider whether your plan reflects these. 
</div>
<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Planning is supported by leadership. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm16leadership"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV13"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm16leadership"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Planning provides for the access and functional needs of the whole school community.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm16community"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV14"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm16community"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Planning considers all settings and all times. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm16items"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV15"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm16items"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 
</div>
</section>

<section>
<div class="question">
17.	Once the plan has been finalized, it is important to share it with the appropriate parties for approval and buy-in. Consider whether you have done the following to share your plan: 
</div>
<div class = "testtable">

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Obtained official approval of the plan from leadership.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm17leadership"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV16"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm17leadership"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Shared the plan with community partners who have a responsibility in the plan (e.g., first responders, local emergency management staff)</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm17partnersplan"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV17"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm17partnersplan"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Shared the plan with other stakeholders that have a role in the plan (i.e., other relevant district, local, regional, and/or state agencies with which the plan will be coordinated).</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm17stakeholdersplan"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV18"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm17stakeholdersplan"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Shared the plan with other organizations that may use the school building(s).</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm17building"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV19"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm17building"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Complied with state and local open records laws in storing and protecting the plan.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm17recordsplan"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV20"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm17recordsplan"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Maintained a record of the people and organizations that receive a copy of the plan.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm17organizationsplan"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV21"
runat="server"
ValidationGroup="vgView7"
ControlToValidate="rblstk12nm17organizationsplan"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 
</div>
</section>


        
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button13" runat="server" Text="Previous"  OnClick="K12PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button14" runat="server" Text="Next" OnClick="K12NextClick" ValidationGroup="vgView7" />
            </div>
        </div>
        </asp:View>
        
        <asp:View ID="ViewStep6" runat="server">
        <h2>Step 6: A high-quality plan is implemented, maintained, and reviewed on a regular basis.</h2>
        
        <div class="intro-note">Everyone involved in the plan needs to know his or her roles and responsibilities before, during, and after an emergency. It is important to use a variety of training components to teach stakeholders about their roles and responsibilities before, during, and after an incident. 
        </div>
        
<section>
<div class="question">18. Does your EOP includes accommodations for the following implementation, maintenance, and review purposes?</div>
<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Holding annual meetings to train all stakeholders on the plan.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm18stakeholdersplan"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV22"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm18stakeholdersplan"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Visiting evacuation sites, reunification sites, media areas, triage areas, etc., with involved parties.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm18parties"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV23"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm18parties"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Giving involved parties appropriate and relevant literature (or quick reference guides, where applicable) on the plan, policies, and procedures. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm18procedures"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV25"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm18procedures"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Posting key information throughout school building(s) so students and staff have easy access to information, and ensuring that information concerning evacuation routes and shelter-in-place procedures and locations is communicated effectively to students, staff, and parents with disabilities, as well as other access and functional needs.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm18needs"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV26"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm18needs"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Familiarizing students and staff with the plan and community partners.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm18partners"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV27"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm18partners"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Training staff on the skills necessary to fulfill their roles. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm18roles"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV28"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm18roles"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>    
</div>

</section>

<div class="intro-note">
A plan must also be practiced. The more stakeholders are trained on the plan, the more effectively they will be able to act before, during, and after an emergency to lessen the impact on life and property. The types of exercises listed below require increasing amounts of planning, time, and resources.
</div>
        
<section>
<div class="question">
19.	Indicate below whether your EOP includes plans for <a id="popupid28" title="The more a plan is practiced and stakeholders are trained on the plan, the more effectively they will be able to act before, during, and after an emergency to lessen the impact on life and property. Exercises provide chances to practice with community partners, and to identify gaps and weaknesses in your plan. The exercises listed here require increasing amounts of planning, time, and resources. Ideally, schools will create an exercise program, building from a tabletop exercise up to a more advanced exercise, like a functional exercise. For more information on exercises, see pp. 21-22 in the <i>School Guide</i>, or visit <a href='http://rems.ed.gov/K12PPStep06.aspx' target='_blank'>http://rems.ed.gov/K12PPStep06.aspx</a>.">executing any of these drills or exercises</a>. Some types of drills are required by states or localities.
</div>


<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"><a id="popupid29" title="Tabletop exercises are small-group discussions that walk through a scenario and the courses of action a school will need to take before, during, and after an emergency to lessen the impact on the school community." >Tabletop exercises</a></div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm19Texercises"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes, plan includes</asp:ListItem>
<asp:ListItem>No, plan doesn’t include</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV29"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm19Texercises"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> <a id="popupid30" title=" During drills, school personnel and community partners use the school grounds and buildings to practice responding to a scenario. Many schools already hold regular evacuation (fire) drills and lockdown drills." >Drills</a> </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm19Drills"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes, plan includes</asp:ListItem>
<asp:ListItem>No, plan doesn’t include</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV30"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm19Drills"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> <a id="popupid31" title="Functional exercises are like drills, but involve multiple partners, and some may be conducted district-wide. Participants react to realistic simulated events (e.g., a bomb threat), and implement the plan and procedures using the Incident Command System. " >Functional exercises</a></div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm19Fexercises"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes, plan includes</asp:ListItem>
<asp:ListItem>No, plan doesn’t include</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV31"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm19Fexercises"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> <a id="popupid32" title="These exercises are the most time-consuming activity in the exercise continuum and are multiagency, multijurisdictional efforts in which all resources are deployed." >Full-scale exercises</a></div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm19FSexercises"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes, plan includes</asp:ListItem>
<asp:ListItem>No, plan doesn’t include</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV32"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm19FSexercises"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 


<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip30" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid29" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true" />
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip31" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid30" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true" />
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip32" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid31" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true" />
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip33" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid32" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true" />
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip29" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid28" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="350" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
</div>

</section>        
 
<section>
<div class="question">
20.	When the plan is exercised, the planning team should evaluate the exercise to gain information and feedback on the success of the exercise (and the plan). Does your EOP incorporate the following criteria to evaluate the plan when exercised?
</div>

<div class = "testtable">       

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">The exercise included community partners. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm20partners"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV33"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm20partners"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">The planning team communicated information about the exercise in advance to avoid confusion and concern. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm20concern"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV34"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm20concern"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">The plan was exercised under different and non-ideal conditions (e.g., times of day, weather conditions, points in the academic calendar, absence of key personnel, and various school events).</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm20events"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV35"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm20events"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Consistent, common emergency management terminology was used.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm20used"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV36"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm20used"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Following the exercise, the planning team debriefed and developed an after-action report that evaluated results, identified gaps or shortfalls, and documented lessons learned. </div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm20learned"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV37"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm20learned"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">The planning team discussed how the school EOP and procedures will be modified, if needed, and specified who has the responsibility for modifying the plan.</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm20theplan"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV38"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm20theplan"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 
</div>
</section>       


<div class="intro-note">
Finally, a plan must be reviewed, revised, and maintained. 
</div>

<section>
<div class="question">
21.	Does your planning team regularly review and revise your plan following these incidents and events?
</div>
<div class = "testtable">

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Actual emergencies</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm21emergencies"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV39"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm21emergencies"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Changes in policy, personnel, organizational structures, processes, facilities, or equipment</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm21equipment"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV40"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm21equipment"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Finalized formal updates of planning guidance or standards</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm21standards"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV41"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm21standards"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Formal exercises</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm21exercises"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV42"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm21exercises"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">Changes in the school and surrounding community</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm21community"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV43"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm21community"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">New or changed threats or hazards</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm21hazards"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV44"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm21hazards"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">New information generated from ongoing assessments</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm21assessments"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV45"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm21assessments"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 
</div>
</section>       


<div class="intro-note">
Planning is a continuous process, even after a plan is approved and published. Plans should evolve as the school and planning team learn lessons, obtain new information and insights, and update priorities. 
</div>
        
        
<section>

<div class="question">
22.	Does your planning team have a process and schedule for reviewing and revising your plan?
</div>
<div class = "testtable">
<fieldset>
<div class="questionBlock">        
<div class="col-lg-10 col-xs-12">
<asp:RadioButtonList
ID="rblstk12nm22question"
runat="server"
RepeatDirection="Vertical"
RepeatLayout="Flow">
<asp:ListItem>Yes </asp:ListItem>
<asp:ListItem>No </asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RV46"
runat="server"
ValidationGroup="vgView8"
ControlToValidate="rblstk12nm22question"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 
</div>
</section>        
        


        
        
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button15" runat="server" Text="Previous"  OnClick="K12PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button16" runat="server" Text="Next" OnClick="K12NextClick" ValidationGroup="vgView8" />

            </div>
        </div>
        </asp:View>
        <asp:View ID="view200" runat="server" >
        <div class="final-message">
        Thank you for completing EOP EVALUATE for K-12 Schools! To access your customized results report, select Download.
        </div>
        
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="btnK12Back" runat="server" Text="Previous"  OnClick="K12PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="lbtnk12PrintReport" runat="server" Text="Download" OnClick="PrintReport" />
            </div>
        </div>
        </asp:View>
    </asp:MultiView>
    
    
    <asp:MultiView ID="mvIHE" runat="server" ActiveViewIndex="0" Visible="false">
        <asp:View ID="View99" runat="server" >
                <h1>For IHEs</h1>
              <p>
Welcome to EOP EVALUATE! This tool is designed to help you evaluate an existing higher ed emergency operations plan (EOP) against the guidelines set forth in the Federal <i>Guide for Developing High-Quality Emergency Operations Plans for Institutions of Higher Education (IHE Guide)</i>. EOP EVALUATE for IHEs is set up to help you determine the extent to which your existing plan aligns with the recommendations made by the U.S. Departments of Education, Homeland Security with the Federal Emergency Management Agency, Justice with the Federal Bureau of Investigation, and Health and Human Services, specifically, the planning principles, six-step planning process for plan development, and the structure of your plan, as set forth in the Federal guidance. The process of completing this tool will take you through the Federal guidance itself, and reading and responding to the prompts provided will help illustrate the concepts and recommendations provided in the <i>IHE Guide</i>. You may use this tool to help identify areas for plan enhancement, recognize strengths in your existing plan, and/or to learn more about the recommendations set forth in the Federal guidance. Please note that completion of this tool requires some familiarity with the <i>IHE Guide</i> and the terminology used therein, and assumes the user possesses extensive knowledge of the content of their higher ed EOP. For more information on the Federal guidance, please visit the Readiness and Emergency Management for Schools (REMS) Technical Assistance (TA) Center’s Website at 
<a href="http://rems.ed.gov/IHEGuideIntro.aspx"target="_blank">http://rems.ed.gov/IHEGuideIntro.aspx</a>.
</p>  
<p>
Upon completing EOP EVALUATE for IHEs, your responses will result in a customized downloadable report containing a variety of additional resources and information relevant to your needs. This tool should take approximately 18-25 minutes to complete. Please note that no information is recorded during this process; your responses are used only to generate your customized report, and no data remains in our system upon completion and download of the report.
</p>
<p>
For assistance using this tool, please contact the REMS TA Center Help Desk by email <a href="mailto:info@remstacenter.org">(info@remstacenter.org)</a> or by telephone (855-781-7367 [REMS]) toll-free Monday through Friday between the hours of 9:00 a.m. and 5:00 p.m., Eastern Time.
</p>
<p>
Let’s get started! Please note that all fields are required. Click NEXT to continue.
</p>

              <h1>Demographic Information</h1>
              
                <p>
                  <span>What type of organization are you from? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </span>
                    <asp:DropDownList ID="ddlIHEorgtype" runat="server">
                    <asp:ListItem >Select One</asp:ListItem>
	                <asp:ListItem>Four-year IHE</asp:ListItem>
	                <asp:ListItem>Two-year IHE</asp:ListItem>
	                <asp:ListItem>Vocational School</asp:ListItem>
	                <asp:ListItem>Proprietary School</asp:ListItem>
	                <asp:ListItem>Community College</asp:ListItem>
                    <asp:ListItem>Online/Virtual University</asp:ListItem>
                    </asp:DropDownList>

                   <asp:RequiredFieldValidator   
        ID="RequiredFieldValidator1122"  
        runat="server"  
        ControlToValidate="ddlIHEorgtype" ForeColor="Red" SetFocusOnError="true" ValidationGroup="vgView2"
        ErrorMessage="*Required"  InitialValue="Select One"  />
                 </p>
                 <p>
                 <span>What is your role within your organization?</span>
                 &nbsp;&nbsp;&nbsp;
                 <span>
                    <asp:DropDownList ID="ddlIHEorgrole" runat="server">
                    <asp:ListItem >Select One</asp:ListItem>
<asp:ListItem>Law Enforcement Officer</asp:ListItem>
<asp:ListItem>Security</asp:ListItem>
<asp:ListItem>Emergency Medical Services</asp:ListItem>
<asp:ListItem>Emergency Management</asp:ListItem>
<asp:ListItem>President</asp:ListItem>
<asp:ListItem>Dean</asp:ListItem>
<asp:ListItem>Administrator</asp:ListItem>
<asp:ListItem>Professor</asp:ListItem>
<asp:ListItem>Academic Advisor</asp:ListItem>
<asp:ListItem>Academic Affairs</asp:ListItem>
<asp:ListItem>Business Office</asp:ListItem>
<asp:ListItem>Central Administration or Designee</asp:ListItem>
<asp:ListItem>Public and Mental Health Practitioner</asp:ListItem>
<asp:ListItem>Counseling and Mental Health Services</asp:ListItem>
<asp:ListItem>Environmental Health and Safety</asp:ListItem>
<asp:ListItem>Facilities and Operations</asp:ListItem>
<asp:ListItem>Food Services</asp:ListItem>
<asp:ListItem>Health Services</asp:ListItem>
<asp:ListItem>Human Resources</asp:ListItem>
<asp:ListItem>Information Technology</asp:ListItem>
<asp:ListItem>Legal Counsel</asp:ListItem>
<asp:ListItem>Public Information Office</asp:ListItem>
<asp:ListItem>Public Safety Operations</asp:ListItem>
<asp:ListItem>Residential Life</asp:ListItem>
<asp:ListItem>Student Affairs</asp:ListItem>
<asp:ListItem>Transportation</asp:ListItem>
<asp:ListItem>International Student Services Office</asp:ListItem>
<asp:ListItem>Other</asp:ListItem>
                    </asp:DropDownList>
                 </span>
                 <asp:RequiredFieldValidator   
        ID="RequiredFieldValidator1123"  
        runat="server"  
        ControlToValidate="ddlIHEorgrole" ForeColor="Red" SetFocusOnError="true" ValidationGroup="vgView2"
        ErrorMessage="*Required"  InitialValue="Select One"  />
                 </p>
                <p>&nbsp;
                    </p>

                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button19" runat="server" Text="Previous" Visible="false" Enabled="false" ForeColor="Gray" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button20" runat="server" Text="Next" OnClick="IHENextClick" ValidationGroup="vgView2" />
            </div>
        </div>
        </asp:View>



       <%-- <asp:View ID="View110" runat="server">
              

                <!-- BEGIN PREV-NEXT NAVIGATION -->
      <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button21" runat="server" Text="Previous"  OnClick="IHEPrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button22" runat="server" Text="Next" OnClick="IHENextClick" ValidationGroup="vgView2" />
            </div>
        </div>
 </asp:View>--%>


        <asp:View ID="View111" runat="server">
        <div class="intro-note">
            <p>
                        EOP EVALUATE will help you determine the extent to which your existing EOP aligns 
                         with the recommendations set forth in the Federal <i>Guide for Developing High-Quality  
                         Emergency Operations Plans (IHE Guide).</i> Use knowledge of the content of 
                         your EOP, and the process your IHE or district underwent to create your EOP, 
                         to respond to the following questions. 
            </p>
        </div>
        <h2>Step 1: A high-quality plan is supported by a collaborative process that includes multiple perspectives.</h2>
        <div class="question">
           
                1. Does your IHE have a <a id="popupid34" title=" For more information on forming a collaborative planning team, see pp. 5-15 in the <i>IHE Guide</i> or visit <a href='http://rems.ed.gov/IHEPPStep01.aspx' target='_blank'>http://rems.ed.gov/IHEPPStep01.aspx</a>" >collaborative planning team</a> that includes 
                representation (i.e., in-person participation OR input by way of consultation OR 
                through incorporation of expertise and resources) from the following entities? 
                Please note, online/virtual universities or smaller IHEs may have single individuals 
                representing multiple stakeholder roles. Indicate below which roles are 
                represented on your collaborative planning team.
               
           </div> 
<div class="testtable">           
<section>

<div class="topic">IHE COMMUNITY</div>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Academic Affairs
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomleaderAffair" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator   
ID="RequiredFieldValidator1128"  
runat="server"  
ControlToValidate="rblstIHEisschcomleaderAffair" ValidationGroup="vgView3"  ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required"  
/> 
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Business Office
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomadminOffice" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1129" runat="server" ValidationGroup="vgView3" ControlToValidate="rblstIHEisschcomadminOffice" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Central Administration or Designee
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomeduDesignee" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1130" runat="server" ValidationGroup="vgView3" ControlToValidate="rblstIHEisschcomeduDesignee" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Counseling and Mental Health Services
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomDistrictService" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1131" runat="server" ValidationGroup="vgView3" ControlToValidate="rblstIHEisschcomDistrictService" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Environmental Health and Safety
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcompsychSafety" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1132" runat="server" ValidationGroup="vgView3" ControlToValidate="rblstIHEisschcompsychSafety" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Facilities and Operations
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomconslorOperation" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1133" runat="server"  ControlToValidate="rblstIHEisschcomconslorOperation" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Food Services
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomnurseService" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1134" runat="server"  ControlToValidate="rblstIHEisschcomnurseService" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Health Services
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomfmanagerHservice" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1135" runat="server"  ControlToValidate="rblstIHEisschcomfmanagerHservice" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Human Resources
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomtmanagerHRes" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1136" runat="server"  ControlToValidate="rblstIHEisschcomtmanagerHRes" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Information Technology
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomfpersonITech" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1137" runat="server"  ControlToValidate="rblstIHEisschcomfpersonITech" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Legal Counsel
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomcstaffCousel" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1138" runat="server"  ControlToValidate="rblstIHEisschcomcstaffCousel" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Public Information Office
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomSROsOffice" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1139" runat="server"  ControlToValidate="rblstIHEisschcomSROsOffice" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required"  ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Public Safety Operations
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomITSpecOperation" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1140" runat="server"  ControlToValidate="rblstIHEisschcomITSpecOperation" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Residential Life
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomfsrepLife" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1141" runat="server"  ControlToValidate="rblstIHEisschcomfsrepLife" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Student Affairs
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomstudentAffair" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1142" runat="server"  ControlToValidate="rblstIHEisschcomstudentAffair" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Transportation
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomfamilyTrans" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1143" runat="server"  ControlToValidate="rblstIHEisschcomfamilyTrans" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
International Student Services Office
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEischcomASchrepOffice" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1144" runat="server"  ControlToValidate="rblstIHEischcomASchrepOffice" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Student and Family Representatives
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisschcomIHErep" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1145" runat="server"  ControlToValidate="rblstIHEisschcomIHErep" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

</section>

<section>
<div class="topic">INTERESTS REPRESENTED</div>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Individuals with Disabilities and Others With Access and Functional Needs
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisINTREPindiv" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator1146" runat="server"  ControlToValidate="rblstIHEisINTREPindiv" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Racial and Ethnic Minorities
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisINTREPracial" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator147" runat="server"  ControlToValidate="rblstIHEisINTREPracial" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Religious
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisINTREPReligious" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator148" runat="server"  ControlToValidate="rblstIHEisINTREPReligious" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Limited English Proficiency
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisINTREPlimited" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator149" runat="server"  ControlToValidate="rblstIHEisINTREPlimited" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
International Students
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisINTREPiheStudents" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator150" runat="server"  ControlToValidate="rblstIHEisINTREPiheStudents" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>
</section> 

<section>
<div class="topic">COMMUNITY PARTNERS</div>
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
First Responders
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisComPart1stResp" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator151" runat="server"  ControlToValidate="rblstIHEisComPart1stResp" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Local Emergency Management Staff
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisComPartlocalStaff" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator152" runat="server"  ControlToValidate="rblstIHEisComPartlocalStaff" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Law Enforcement Officers
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisComPartLawofficer" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator153" runat="server"  ControlToValidate="rblstIHEisComPartLawofficer" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Emergency Medical Services Personnel
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisComPartEMSPersonnel" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator154" runat="server"  ControlToValidate="rblstIHEisComPartEMSPersonnel" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Fire Officials
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisComPartFireOfficial" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator155" runat="server"  ControlToValidate="rblstIHEisComPartFireOfficial" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Public Health Practitioners
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisComPartPHPract" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator156" runat="server" ValidationGroup="vgView3"   ControlToValidate="rblstIHEisComPartPHPract" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Mental Health Practitioners
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisComPartMHPract" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator157" runat="server" ValidationGroup="vgView3"  ControlToValidate="rblstIHEisComPartMHPract" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required"/>
</div>
</div>           
</fieldset>

</section>

<section>
<div class="topic">ADDITIONAL PARTNERS WITH ROLE IN IHE EMERGENCY MANAGEMENT</div>
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Policy Makers and Local Elected Officials
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisAPRSEMPMLEofficial" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator158" runat="server"  ControlToValidate="rblstIHEisAPRSEMPMLEofficial" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Community-based Religious Organizations
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisAPRSEMCBRorganize" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator159" runat="server"  ControlToValidate="rblstIHEisAPRSEMCBRorganize" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Business Partners
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisAPRSEMBusPart" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator160" runat="server"  ControlToValidate="rblstIHEisAPRSEMBusPart" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3"  />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Community-based Youth Organizations
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisAPRSEMCBYOrganize" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator161" runat="server"  ControlToValidate="rblstIHEisAPRSEMCBYOrganize" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
State and Federal Law Enforcement (FBI, DEA, ATF, etc.) and Homeland Security 
Officials
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisAPRSEMSFLEHSOfficial" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator162" runat="server"  ControlToValidate="rblstIHEisAPRSEMSFLEHSOfficial" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
District and State Attorney General
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisAPRSEMDSAGeneral" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator163" runat="server"  ControlToValidate="rblstIHEisAPRSEMDSAGeneral" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Local Social Services Departments
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisAPRSEMLSSDepart" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator164" runat="server"  ControlToValidate="rblstIHEisAPRSEMLSSDepart" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3"  />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Disaster Organizations (e.g., American Red Cross)
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisAPRSEMDisOrganize" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator165" runat="server"  ControlToValidate="rblstIHEisAPRSEMDisOrganize" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Media
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEisAPRSEMMedia" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator166" runat="server"  ControlToValidate="rblstIHEisAPRSEMMedia" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView3"  />
</div>
</div>
</fieldset>
</section>
 
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip34" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid34" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>       
</div>           

<div class="question last-section">
2.	When starting out, did your collaborative planning team form a shared approach to developing the EOP, including each of the following:
</div>

<div class = "testtable">
<section>
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
a.	Developing a common framework in which all parties understand each other’s vocabulary, command structure, culture, and use of common critical concepts and principles, such as the National Incident Management System (NIMS).
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEis2NIMS" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator167" runat="server" ValidationGroup="vgView3" ControlToValidate="rblstIHEis2NIMS" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
b.	Establishing each member’s unique role and responsibilities.
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEis2UniRole" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator168" runat="server" ValidationGroup="vgView3" ControlToValidate="rblstIHEis2UniRole" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
c.	Determining a regular schedule of meetings.
</div>                <div class="col-lg-4 col-xs-12">
                    <asp:RadioButtonList ID="rblstIHEis2RegMeeting" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
               		 <asp:RequiredFieldValidator ID="RequiredFieldValidator122" runat="server" ValidationGroup="vgView3" ControlToValidate="rblstIHEis2RegMeeting" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>

</section>

</div>
        
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button23" runat="server" Text="Previous"  OnClick="IHEPrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button24" runat="server" Text="Next" OnClick="IHENextClick" ValidationGroup="vgView3" />
            </div>
        </div>
        </asp:View>
        <asp:View ID="View9" runat="server">
        <h2>Step 2: A high-quality plan is customized based on a data-informed understanding of the situation.</h2>
        


        <%--<div class="intro-note">
        Next, thinking about the list of threats and hazards identified, did your planning team assess the risk posed by the identified threats and hazards? Evaluating risk entails understanding the probability that the specific threat or hazard will occur; the effects it will likely have, including the severity of impact; the time the school will have to warn students and staff about the threat or hazard; and how long it may last. Data must be collected to help assess the risk posed. 
</div>--%>


<section>
<div class="question">
3. Did your planning team conduct assessments of your IHE site to better understand threats and hazards that your IHE may face? 
       Examples of recommended assessments include:

</div>
<div class = "testtable">
<fieldset>
<div class="questionBlock">

<div class="col-lg-4 col-xs-12">
<ul>
<li><a id="popupid36" title="A site assessment examines the safety, accessibility, and emergency preparedness of the buildings, facilities, and grounds. This assessment includes, but is not limited to, a review of building access control measures, visibility around the exterior of the buildings, compliance with applicable architectural standards for individuals with disabilities and others with access and functional needs, structural integrity of the buildings, and emergency vehicle access.">Site Assessments</a> 
</li>
<li><a id="popupid37" title="In a nurturing, inclusive environment, members of a community are more likely to succeed, feel safe, and report threats. If a student survey is used to assess culture and climate, student privacy must be protected. A range of personnel across the IHE can assist in the assessment of climate, including counselors and mental health staff.">Climate Assessments</a> 
</li>
<li><a id="popupid38" title="A campus threat assessment analyzes campus members’ communication and behaviors to determine whether or not a member may pose a threat. These assessments must be based on fact, must comply with applicable privacy, civil rights, and other applicable laws, and are often conducted by multidisciplinary threat assessment teams.">Threat Assessments</a>
</li>
<li><a id="popupid39" title="A capacity assessment examines the capabilities of students, faculty, and staff as well as the services and material resources of community partners. This is used to identify individuals on campus with applicable skills (e.g., first aid certification) and inventory equipment and supplies (e.g., evacuation chairs for individuals with disabilities and others with access and functional needs).">Capacity Assessments</a>
</li>
</ul>
</div>


<div class="col-lg-6 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm5Highquality" RepeatDirection="Vertical" runat="server" 
RepeatLayout="Flow" >
<asp:ListItem>Yes, multiple assessments were conducted</asp:ListItem>
<asp:ListItem>Yes, two assessments were conducted</asp:ListItem>
<asp:ListItem>No, only one assessment was completed</asp:ListItem> 
<asp:ListItem>No, no assessments at all were completed</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>

<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator129" runat="server"  ControlToValidate="rblstIHEnm5Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView4" />
</div> 
</div>
</fieldset>

 <telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip36" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid36" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip37" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid37" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip38" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid38" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip39" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid39" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>       
</div>        
</section>            


<section>       
<div class="question">
4. Did your planning team identify a comprehensive list of both current and 
historical <a id="popupid35" title="For a list of potential threats and hazards IHEs may face, depending on their unique locale, see p. 45 in the <i>IHE Guide</i> or visit <a href='http://rems.ed.gov/IHEThreatAndHazardAnnex.aspx' target='_blank'>http://rems.ed.gov/IHEThreatAndHazardAnnex.aspx</a>.">threats and hazards</a> in the IHE and the surrounding community? A 
list is comprehensive when it is informed by data from the following sources: 
</div>

<div class="testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<ul>
<li>Assessment data from site assessments, climate assessments, threat assessments, capacity assessments, and other types of assessments  </li>
<li>Team members’ own knowledge, expertise, and existing data collection sources.</li>
<li>Institutional data, including crime statistics and crime logs, as required under the <a href="http://rems.ed.gov/IHECleryAct.aspx" target="_blank"><i>Jeanne Clery Disclosure of Campus Security Policy and Campus Crime Statistics Act</i></a> (<i>Clery Act</i>)</li>
<li>Local agencies, including, but not limited to:
<ul>
<li>Emergency Management Offices</li>
<li>Fire Department </li>
<li>Police Department</li>
<li>Local Organizations and Community Groups (e.g., local chapter of the American Red Cross, Community Emergency Response Team)</li>
<li>Utilities</li>
<li>Businesses</li>
</ul>
</li>
<li>State agencies</li>
<li>Federal agencies</li>
</ul>
</div>

<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm3Highquality" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes, very comprehensive</asp:ListItem>
<asp:ListItem>Yes, somewhat comprehensive</asp:ListItem>
<asp:ListItem>No, somewhat uncomprehensive</asp:ListItem> 
<asp:ListItem>No, not very comprehensive</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>  

<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator123" runat="server"  ControlToValidate="rblstIHEnm3Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView4" />
</div>
</div>
</fieldset>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip35" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid35" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="400" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

</div>   
</section>



<section>
<div class="question">

5. Evaluating the risk posed by threats and hazards entails understanding the probability that a specific threat or hazard will occur; the effects it will likely have, 
   including the severity of impact; the time the IHE will have to warn students and staff about a threat or hazard; and how long it may last. Did your planning team 
   evaluate the risks posed by identified threats and hazards?

        </div>
<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-10 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm4Highquality" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div> 
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator128" runat="server"  ControlToValidate="rblstIHEnm4Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView4" />
</div>
</div>
</fieldset> 
 </div>     
</section>

 
 <section>
            
<div class="question">
6. After evaluating the risk posted by the identified threats and hazards, did your planning team compare the risks and vulnerabilities of the identified threats and 
   hazards, and <a id="popupid40" title="For an example method of how to prioritize threats and hazards, see pp. 19-20 in the <i>IHE Guide</i> or visit <a href='http://rems.ed.gov/IHEPPStep02.aspx' target='_blank'>http://rems.ed.gov/IHEPPStep02.aspx</a>."> create a prioritized list</a> of threats and hazards to address in your EOP? 
</div>
 <telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip40" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid40" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-10 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm6Highquality" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div> 
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator130" runat="server"  ControlToValidate="rblstIHEnm6Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView4" />
</div>
</fieldset>
</div>
</div>
</section>

                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button25" runat="server" Text="Previous"  OnClick="IHEPrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button26" runat="server" Text="Next" OnClick="IHENextClick" ValidationGroup="vgView4" />
            </div>
        </div>
        </asp:View>
        <asp:View ID="View10" runat="server">
        <h2>Step 3: A high-quality plan articulates clear goals and objectives to protect persons and property.</h2>

<section>
    <div class="question">
       7.	Once your planning team identified a prioritized list of threats and hazards to address in your EOP, they should have developed goals and objectives for addressing each threat or hazard.

    </div>
        
 <div class="testtable">       
<fieldset>
<div class="questionBlock">       
<div class="col-lg-6 col-xs-12">        
a.	Did your planning team develop <a id="popupid99" title="Goals are broad, general statements that indicate the desired outcome in response to the threat or hazard identified by planners.<br/><br/><u>Example:</u><br/><b>Goal 1 (before):</b> Prevent a fire from occurring in IHE-governed student housing facilities. <br/><b>Goal 2 (during):</b> Protect all persons from injury and property from damage by the fire. <br/><b>Goal 3 (after):</b> Provide necessary medical attention to those in need."><b>at least three goals</b></a> for addressing each threat or hazard, indicating the desired outcome for 1) before, 2) during, and 3) after the threat or hazard? 
</div>  
<div class="col-lg-4 col-xs-12">  
<asp:RadioButtonList ID="rblstIHEnm8aHighquality" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>  
<div class="col-lg-2 col-xs-12">  
<asp:RequiredFieldValidator ID="RequiredFieldValidator132" runat="server" ValidationGroup="vgView5" ControlToValidate="rblstIHEnm8aHighquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />     
</div>  
</div>  
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">  
b.	Did your planning team develop <a id="popupid910" title="Objectives are specific, measurable actions that are necessary to achieve the goals. Often, planners will need to identify multiple objectives in support of a single goal. <br /><br/><u>Example:</u> <br /><b>Goal 1 (before):</b> Prevent a fire from occuring in IHE-governed student housing facilities.<br /><b>Objective 1.1:</b> Provide fire prevention training to all students and resident advisors who use combustible materials or equipment.<br /> <b>Objective 1.2:</b> Store combustible materials in fireproof containers or rooms."><b>multiple objectives</b></a> (specific, measurable actions that are necessary to achieve the goals) for each goal? 
</div>
<div class="col-lg-4 col-xs-12">  	
<asp:RadioButtonList ID="rblstIHEnm8bHighquality" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>

<div class="col-lg-2 col-xs-12">  
<asp:RequiredFieldValidator ID="RequiredFieldValidator133" runat="server" ValidationGroup="vgView5" ControlToValidate="rblstIHEnm8bHighquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>
 
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip18" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid99" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip> 

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip20" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid910" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip> 
    
 </div>           
</section>            
            
<section>            
            
<div class="question">
8.	Once your team compiled the objectives for the prioritized threats and hazards, were certain critical <a id="popupid911" title="Functions are activities that apply to one or more threat or hazard. Examples of these cross-cutting functions include: evacuating; providing medical care; and accounting for all students, faculty, staff, and visitors."><b>functions</b></a> identified that apply to more than one threat or hazard (e.g., evacuating; providing medical care; accounting for all students, faculty, staff, and visitors)?
</div>
<div class = "testtable">      
<fieldset>
<div class="questionBlock">
<div class="col-lg-10 col-xs-12">  	
<asp:RadioButtonList ID="rblstIHEnm9Highquality" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator134" runat="server" ValidationGroup="vgView5" ControlToValidate="rblstIHEnm9Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div> 
</div>
</fieldset>           
                   
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip41" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid911" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
          
</div>           
 </section>           
        <div class="intro-note">
        The Federal guidance recommends developing three goals for each function, including the desired outcome for 1) before, 2) during, and 3) after the function has been executed. These commonly occurring functions should have formed the basis for the Functional Annexes component of your higher ed EOP. 
        </div>

                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button27" runat="server" Text="Previous"  OnClick="IHEPrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button28" runat="server" Text="Next" OnClick="IHENextClick" ValidationGroup="vgView5" />
            </div>
        </div>
        </asp:View>
        <asp:View ID="View11" runat="server">
        <h2>Step 4: A high-quality plan prescribes clear courses of action for accomplishing objectives.</h2>
        <div class="intro-note">
        Courses of action address the what, who, when, where, why, and how for each threat, hazard, and function. 
        </div>
  
            
 <section>
      <div class="question">
    9.	Did your planning team use <a id="popupid98" title="What is scenario-based planning? As defined in FEMA’s Community Preparedness Guide (CPG) 101 (2010), &quot;This approach starts with building a scenario for a hazard or a threat. Then, planners analyze the impact of the scenario to determine appropriate courses of action. Planners typically use this planning concept to develop planning assumptions, primarily for hazard- or threat-specific annexes to a basic plan.&quot;  <a href='http://www.fema.gov/media-library-data/20130726-1828-25045-0014/cpg_101_comprehensive_preparedness_guide_developing_and_maintaining_emergency_operations_plans_2010.pdf' target='_blank'>http://www.fema.gov/media-library-data/20130726-1828-25045-0014/cpg_101_comprehensive_preparedness_guide_developing_and_maintaining_emergency_operations_plans_2010.pdf.</a>" >scenario-based planning</a> to consider your identified threats and hazards? 
       </div>

<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-10 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm7Highquality" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator131" runat="server" ValidationGroup="vgView5" ControlToValidate="rblstIHEnm7Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div> 
</div>
</fieldset> 

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip17" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid98" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="800" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

</div>       
  </section>          
                        
                  
<section>
<div class="question">
10.	Do the courses of action for each threat, hazard, and function include the following information: 
</div>
<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> 
What action should be taken
</div>
<div class="col-lg-4 col-xs-12"> 
<asp:RadioButtonList ID="rblstIHEis10taken" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator135" runat="server"  ControlToValidate="rblstIHEis10taken" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView6"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> 
Who is responsible for the action
</div>
<div class="col-lg-4 col-xs-12"> 
<asp:RadioButtonList ID="rblstIHEis10responsibleaction" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator136" runat="server"  ControlToValidate="rblstIHEis10responsibleaction" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView6"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> 
When the action takes place
</div>
<div class="col-lg-4 col-xs-12"> 
<asp:RadioButtonList ID="rblstIHEis10takeplace" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator137" runat="server"  ControlToValidate="rblstIHEis10takeplace" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView6" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> 
How long does the action take and how much time is actually available
</div>
<div class="col-lg-4 col-xs-12"> 
<asp:RadioButtonList ID="rblstIHEis10available" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator138" runat="server"  ControlToValidate="rblstIHEis10available" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView6"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> 
What must happen prior to the action
</div>
<div class="col-lg-4 col-xs-12"> 
<asp:RadioButtonList ID="rblstIHEis10prioraction" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator139" runat="server"  ControlToValidate="rblstIHEis10prioraction" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView6"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> 
What must happen after the action
</div>
<div class="col-lg-4 col-xs-12"> 
<asp:RadioButtonList ID="rblstIHEis10afteraction" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator140" runat="server"  ControlToValidate="rblstIHEis10afteraction" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView6"/>
</div>
</div> 
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> 
Resources necessary for the action 	
</div>
<div class="col-lg-4 col-xs-12"> 
<asp:RadioButtonList ID="rblstIHEis10necessaryaction" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator141" runat="server"  ControlToValidate="rblstIHEis10necessaryaction" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView6"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> 
Consideration for how this action will affect specific populations, such as individuals with disabilities and others with access and functional needs
</div>
<div class="col-lg-4 col-xs-12"> 
<asp:RadioButtonList ID="rblstIHEis10needs" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator142" runat="server"  ControlToValidate="rblstIHEis10needs" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView6"/>
</div>
</div>
</fieldset>
</div>
</section>        
        

             <div class="intro-note">
                Once your planning team has developed courses of action, they should have compared the costs and benefits of each proposed course of action against the goals and objectives. Based on this comparison, planners should have selected preferred courses of action to move forward in the planning process, and then identified the resources necessary to accomplish each course of action without regard to resource availability. After requirements are identified, they can be matched with existing resources, and any gaps in resources can be identified and addressed.
           
                <p class="first-section">
                The outcome of Step 4 should have been the creation of your plan’s annexes: 
                </p>
                <ul class="first-section">
                <li>Goals, objectives, and courses of action for threats and hazards form the Threat- and Hazard-Specific Annexes section of the EOP.</li>
                <li>Goals, objectives, and courses of action for functions will be contained in the Functional Annexes section of the EOP.</li>
                </ul>
      </div>  
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button29" runat="server" Text="Previous"  OnClick="IHEPrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button30" runat="server" Text="Next" OnClick="IHENextClick" ValidationGroup="vgView6" />
            </div>
        </div>
        </asp:View>
        <asp:View ID="View12" runat="server">
        
        <h2>Step 5: A high-quality plan is structured, written, reviewed, and shared to communicate clearly the essential components of the IHE’s approach to emergency management.</h2>
        
<div class="intro-note">
In Step 5, the planning team develops a draft of the higher ed EOP using the courses of action developed in Step 4. The Federal guidance recommends a traditional format that can be tailored to meet individual IHE needs. This includes three major sections: The Basic Plan, Functional Annexes, and Threat- and Hazard-Specific Annexes.
</div>

<section>

<div class="question">
11.	<a id="popupid42" title="For more information on the recommended traditional format for an EOP see pp.31-36 in the <i>IHE Guide</i> or visit <a href='http://rems.ed.gov/IHEBasicPlan.aspx' target='_blank'>http://rems.ed.gov/IHEBasicPlan.aspx</a>." >The Basic Plan</a> section of the IHE EOP provides an overview of the IHE’s approach to emergency operations and includes:
</div>


<div class="testtable">
<fieldset>
<div class="questionBlock">

<div class="col-lg-4 col-xs-12">
<ul>
<li><a id="popupid43" title="Introductory material can enhance accountability with community partners. This section typically includes:<ul><li>Cover Page</li><li>Promulgation Document and Signatures Page</li><li>Approval and Implementation Page</li><li>Record of Changes</li><li>Record of Distribution</li><li>Table of Contents </li></ul>" > Introductory Material</a></li>
<li><a id="popupid44" title="The purpose sets the foundation for the rest of the higher ed EOP—it is a general statement of what the EOP is meant to do. The situation section explains why a higher ed EOP is necessary." > Purpose and Situation Overview</a></li>
<li><a id="popupid45" title="The Concept of Operations section explains the authorized IHE administrator’s intent with regard to an operation; it is designed to give an overall picture of how the IHE will protect students, faculty, staff, and visitors." > Concept of Operations</a></li>
<li><a id="popupid46" title="This section provides an overview of the broad roles and responsibilities of IHE faculty and staff, students, families, first responders, local emergency management, and community partners, and of organizational functions during all emergencies." > Organization and Assignment of Responsibilities</a></li>
<li><a id="popupid47" title="This section describes the framework for all direction, control, and coordination activities." > Direction, Control, and Coordination</a></li>
<li><a id="popupid48" title="This section addresses the role of information in the successful implementation of the activities that occur before, during, and after an emergency." > Information Collection, Analysis, and Dissemination</a></li>
<li><a id="popupid49" title="This section describes the critical training and exercise activities the IHE will use in support of the plan." > Training and Exercises</a></li>
<li><a id="popupid50" title="This section covers general support requirements and the availability of services and support for all types of emergencies, as well as general policies for managing resources." > Administration, Finance, and Logistics</a></li>
<li><a id="popupid51" title="This section discusses the overall approach to planning and the assignment of plan development and maintenance responsibilities." > Plan Development and Maintenance</a></li>
<li><a id="popupid52" title="This section provides the legal basis for emergency operations and activities." > Authorities and References</a></li>
</ul>
</div>
<div class="col-lg-6 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm11Highquality" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>My IHE EOP includes nearly all of these recommended elements.</asp:ListItem>
<asp:ListItem>My IHE EOP includes most of these recommended elements.</asp:ListItem>
<asp:ListItem>My IHE EOP includes some of these recommended elements.</asp:ListItem>
<asp:ListItem>My IHE EOP includes few of these recommended elements. </asp:ListItem>
</asp:RadioButtonList>       
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator143" runat="server"  ControlToValidate="rblstIHEnm11Highquality" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip42" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid42" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip43" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid43" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip44" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid44" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip45" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid45" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip46" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid46" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip47" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid47" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip48" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid48" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip49" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid49" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip50" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid50" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip51" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid51" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip52" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid52" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip> 
</div>
</section>


<section>
<div class="question">
12.	The <a id="popupid53" title="Functional annexes focus on critical operational functions and the courses of action developed to carry them out. For more information on these 10 functional annexes that IHEs should address in developing a comprehensive, high-quality EOP see pp. 36-44 in the <i>IHE Guide</i> or visit <a href='http://rems.ed.gov/IHEFuncAnnex.aspx' target='_blank'>http://rems.ed.gov/IHEFuncAnnex.aspx</a>. " >Functional Annexes</a> section details the goals, objectives, and courses of action of functions (e.g., evacuation, communications, recovery) that apply across multiple threats or hazards. The Federal guidance recommends, at a minimum, the following Functional Annexes be included in your EOP. Indicate whether your EOP contains the following:
</div>
<div class="col-lg-12 col-xs-12 first-section topic">
FUNCTIONAL ANNEXES
</div>

<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12"> Evacuation</div> 
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList
ID="rblstIHEis12Evacuation"
runat="server"
RepeatDirection="Horizontal"
RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>                
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator
ID="RequiredFieldValidator144"
runat="server"
ValidationGroup="vgView6"
ControlToValidate="rblstIHEis12Evacuation"
ForeColor="Red"
SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset> 
     
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Deny Entry or Closing (Lockdown)
</div> 
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis12Lockdown" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator145" runat="server"  ControlToValidate="rblstIHEis12Lockdown" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Shelter-in-Place/Secure-in-Place
</div> 
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis12ShelterinPlace" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator146" runat="server"  ControlToValidate="rblstIHEis12ShelterinPlace" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>  


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Accounting for All Persons
</div> 
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis12AccountingAllPersons" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV147" runat="server"  ControlToValidate="rblstIHEis12AccountingAllPersons" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Communications and Notification
</div> 
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis12ComWarning" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV148" runat="server"  ControlToValidate="rblstIHEis12ComWarning" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset> 

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Continuity of Operations (COOP)
</div> 
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis12COOP" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV149" runat="server"  ControlToValidate="rblstIHEis12COOP" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Recovery
</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis12Recovery" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV150" runat="server"  ControlToValidate="rblstIHEis12Recovery" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>  

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Public Health, Medical, and Mental Health
</div> 
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis12PHMMHealth" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV151" runat="server"  ControlToValidate="rblstIHEis12PHMMHealth" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Security
</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis12Security" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV152" runat="server"  ControlToValidate="rblstIHEis12Security" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Rapid Assessment
</div>
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis12ReunificationAssessment" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV153" runat="server"  ControlToValidate="rblstIHEis12ReunificationAssessment" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Do you have additional annexes in your plan that are not on this list?
</div> 
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis12onlist" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV154" runat="server"  ControlToValidate="rblstIHEis12onlist" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip53" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid53" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="350" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>     

</div>        
</section>        



<section>
<div class="question">
13. The Threat- and Hazard-Specific Annexes section of the EOP specifies the goals, 
objectives, and courses of action that a school will follow to address a particular type of 
threat or hazard (e.g., hurricane, active shooter situation). While each school’s threat- or
 hazard-specific annexes will vary based on the school planning team’s selection of threats 
 and hazards that are specific to the school, the team should still consider the broader 
 categories listed below. Did your EOP planning consider the following broader categories of 
 threats and hazards?
</div>
<div class="topic first-section">THREAT- OR HAZARD-SPECIFIC ANNEXES</div>
<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<a id="popupid54" title="Examples of <b>Natural Hazards</b> may include:<ul><li>Earthquakes</li><li>Tornadoes</li><li>Lightning</li><li>Severe wind</li><li>Hurricanes</li><li>Floods</li><li>Wildfires</li><li>Extreme temperatures</li><li>Landslides or mudslides</li><li>Tsunamis</li><li>Volcanic eruptions</li><li>Winter precipitation</li><li>Infectious diseases, such as pandemic influenza,extensively drug-resistant tuberculosis, <i>Staphylococcus aureus,</i> and meningitis</li><li>Contaminated food outbreaks, including <i>salmonella, botulism, and E. coli</i></li></ul> " >Natural Hazards</a>
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis13NHazards" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV155" runat="server"  ControlToValidate="rblstIHEis13NHazards" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<a id="popupid55" title="Examples of <b>Technological Hazards</b> may include:<ul><li>Explosions or accidental release of toxins from industrial plants</li><li>Accidental release of hazardous materials from within the IHE, such as gas leaks or laboratory spills </li><li>Hazardous materials releases from major highways or railroads </li><li>Radiological releases from nuclear power stations  </li><li>Dam failure</li><li>Power failure</li><li>Water failure</li><li>Fire</li></ul> " >Technological Hazards</a>
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis13THazards" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV156" runat="server"  ControlToValidate="rblstIHEis13THazards" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<a id="popupid56" title="Examples of <b>Adversarial, Incidental, and Human-caused Threats</b> may include:<ul><li>Arson</li><li><i>Active shooters</i></li><li>Criminal threats or actions</li><li>Gang violence</li><li>Bomb threats</li><li>Domestic violence and abuse</li><li>Cyber attacks</li><li>Suicide</li></ul>" >Adversarial, Incidental, and Human-caused Threats</a>
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEis13Threats" runat="server" 
RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV157" runat="server"  ControlToValidate="rblstIHEis13Threats" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>


<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip54" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid54" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip55" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid55" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip56" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid56" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
</div>
</section>

<section>
<div class="question">
14.	When writing the plan, the following guidelines are recommended. Indicate whether these are reflected in your plan. 
</div>
<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Important information is summarized with checklists and visual aids, such as maps and flowcharts.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm14flowcharts" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Reflected in plan</asp:ListItem>
<asp:ListItem>Not reflected in plan</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV158" runat="server"  ControlToValidate="rblstIHEnm14flowcharts" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Language is written clearly, avoiding jargon, minimizing the use of abbreviations, and using short sentences and the active voice.</td>
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm14activevoice" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Reflected in plan</asp:ListItem>
<asp:ListItem>Not reflected in plan</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV159" runat="server"  ControlToValidate="rblstIHEnm14activevoice" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Plan uses a logical, consistent structure that makes it easy for users to grasp the rationale for the sequence of the information and to scan for the information they need.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm14theyneed" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Reflected in plan</asp:ListItem>
<asp:ListItem>Not reflected in plan</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV160" runat="server"  ControlToValidate="rblstIHEnm14theyneed" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Enough detail is provided to convey an easily understood plan that is actionable. 
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm14actionable" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Reflected in plan</asp:ListItem>
<asp:ListItem>Not reflected in plan</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV161" runat="server"  ControlToValidate="rblstIHEnm14actionable" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
The plan is presented through accessible tools and documents (i.e., accessible Websites, digital text that can be converted to audio or Braille).
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm14audioBraille" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Reflected in plan</asp:ListItem>
<asp:ListItem>Not reflected in plan</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV162" runat="server"  ControlToValidate="rblstIHEnm14audioBraille" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7" />
</div>
</div>
</fieldset>
</div>
</section>


<div class="intro-note">
Once the plan is written, it is important to review it for compliance with applicable laws and for its usefulness in practice.
</div>

<section>
<div class="question">
15.	The following measures can help determine if a plan is of high quality. Indicate the extent to which your plan aligns with each of these criteria.
</div>
<div class = "testtable">
<div class="col-lg-12 col-xs-12 subquestion">
<b>The plan is <i>adequate</i> if it:</b>
</div>
<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Identifies and addresses critical courses of action effectively.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15effectively" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV163" runat="server"  ControlToValidate="rblstIHEnm15effectively" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Can accomplish the assigned function.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15assignedfunction" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RV164" runat="server"  ControlToValidate="rblstIHEnm15assignedfunction" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Contains assumptions that are valid and reasonable.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15reasonable" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator79" runat="server"  ControlToValidate="rblstIHEnm15reasonable" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<div class="col-lg-12 col-xs-12 subquestion">
<b>The plan is <i>feasible</i> if:</b>
</div>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
The IHE can accomplish the assigned function and critical tasks by using available resources within the time contemplated by the plan.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15theplan" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator80" runat="server"  ControlToValidate="rblstIHEnm15theplan" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<div class="col-lg-12 col-xs-12 subquestion">
<b>The plan is <i>acceptable </i> if it:</b>
</div>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Meets the requirements driven by a threat or hazard.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15threathazard" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator81" runat="server"  ControlToValidate="rblstIHEnm15threathazard" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Meets costs and time limitations.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15limitations" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator82" runat="server"  ControlToValidate="rblstIHEnm15limitations" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Is consistent with the law.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15thelaw" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator83" runat="server"  ControlToValidate="rblstIHEnm15thelaw" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<div class="col-lg-12 col-xs-12 subquestion">
<b>The plan is <i>complete </i> if it:</b>
</div>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Incorporates all courses of action to be accomplished for all selected threats and hazards and identified functions.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15idfunctions" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator84" runat="server"  ControlToValidate="rblstIHEnm15idfunctions" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Integrates the needs of the whole IHE community.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15schcommunity" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator85" runat="server"  ControlToValidate="rblstIHEnm15schcommunity" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Provides a complete picture of what should happen, when, and at whose direction. 
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15WhoseDirection" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator86" runat="server"  ControlToValidate="rblstIHEnm15WhoseDirection" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Estimates time for achieving objectives, with safety remaining as the utmost priority.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15utmostpriority" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator87" runat="server"  ControlToValidate="rblstIHEnm15utmostpriority" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Identifies success criteria and a desired end state.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15endstate" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator88" runat="server"  ControlToValidate="rblstIHEnm15endstate" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>


<div class="col-lg-12 col-xs-12 subquestion">
<b>A plan must <i>comply</i>:</b>
</div>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
With applicable state and local requirements.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm15localrequirements" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator89" runat="server"  ControlToValidate="rblstIHEnm15localrequirements" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>
</div>
</section>

<section>
<div class="question">
16.	A high-quality plan should also incorporate the following planning principles. Consider whether your plan reflects these. 
</div>
<div class = "testtable">

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Planning is supported by leadership.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm16leadership" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator90" runat="server"  ControlToValidate="rblstIHEnm16leadership" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Planning provides for the access and functional needs of the whole IHE community.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm16community" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator91" runat="server"  ControlToValidate="rblstIHEnm16community" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Planning considers all settings and all times.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm16items" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Plan fully aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan somewhat aligns with this criterion</asp:ListItem>
<asp:ListItem>Plan does not align with this criterion</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator92" runat="server"  ControlToValidate="rblstIHEnm16items" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

</div>
</section>

<section>
<div class="question">
17.	Once the plan has been finalized, it is important to share it with the appropriate parties for approval and buy-in. 
    Consider whether you have done the following to share your plan: 
</div>
<div class = "testtable">

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<td>Obtained official approval of the plan from leadership. </td>
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm17leadership" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator93" runat="server"  ControlToValidate="rblstIHEnm17leadership" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Shared the plan with community partners who have a responsibility in the plan (e.g., first responders, local emergency management staff)
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm17partnersplan" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator94" runat="server"  ControlToValidate="rblstIHEnm17partnersplan" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Shared the plan with other stakeholders that have a role in the plan (i.e., other relevant local, regional, and/or state agencies with which the plan will be coordinated).
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm17stakeholdersplan" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator95" runat="server"  ControlToValidate="rblstIHEnm17stakeholdersplan" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Shared the plan with other organizations that may use the campus buildings, facilities, and grounds.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm17building" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator96" runat="server"  ControlToValidate="rblstIHEnm17building" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Complied with state and local open records laws in storing and protecting the plan. 
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm17recordsplan" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator97" runat="server"  ControlToValidate="rblstIHEnm17recordsplan" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Maintained a record of the people and organizations that receive a copy of the plan. </div><div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm17organizationsplan" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator98" runat="server"  ControlToValidate="rblstIHEnm17organizationsplan" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView7"/>
</div>
</div>
</fieldset>
</div>
</section>

        
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button31" runat="server" Text="Previous"  OnClick="IHEPrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button32" runat="server" Text="Next" OnClick="IHENextClick" ValidationGroup="vgView7" />
            </div>
        </div>
        </asp:View>
        <asp:View ID="View8" runat="server">
        <h2>Step 6: A high-quality plan is implemented, maintained, and reviewed on a regular basis.</h2>
        
        <div class="intro-note">Everyone involved in the plan needs to know his or her roles and responsibilities before, during, and after an emergency. It is important to use a variety of training components to teach stakeholders about their roles and responsibilities before, during, and after an incident. 
        </div>
        
<section>
<div class="question">
18. Does your EOP include accommodations for the following implementation, maintenance, and review purposes?
</div>

<div class = "testtable">

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Holding annual meetings to train all stakeholders on the plan.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm18stakeholdersplan" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator99" runat="server" ValidationGroup="vgView8" ControlToValidate="rblstIHEnm18stakeholdersplan" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Visiting evacuation sites, reunification sites, media areas, triage areas, etc., with involved parties.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm18parties" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator100" runat="server"  ControlToValidate="rblstIHEnm18parties" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Giving involved parties appropriate and relevant literature (or quick reference guides, where applicable) on the plan, policies, and procedures.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm18procedures" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator101" runat="server"  ControlToValidate="rblstIHEnm18procedures" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
    Posting key information throughout the IHE building(s) and grounds so students, faculty, and staff have easy access to information, and ensuring that information concerning evacuation routes and shelter-in-place procedures and locations is communicated effectively to students, faculty, and staff with disabilities, as well as others with access and functional needs.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm18needs" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator102" runat="server"  ControlToValidate="rblstIHEnm18needs" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Familiarizing students, faculty, and staff with the plan and community partners.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm18partners" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator103" runat="server"  ControlToValidate="rblstIHEnm18partners" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
	Training staff on the skills necessary to fulfill their roles.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm18roles" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes, reflected in plan</asp:ListItem>
<asp:ListItem>No, not reflected in plan</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator104" runat="server"  ControlToValidate="rblstIHEnm18roles" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>
</div>
</section>    
        

<div class="intro-note">
A plan must also be practiced. The more stakeholders are trained on the plan, the more effectively they will be able to act before, during, and after an emergency to lessen the impact on life and property. The types of exercises listed below require increasing amounts of planning, time, and resources.
</div>


<section>
<div class="question">
19.	Indicate below whether your EOP includes plans for<a id="popupid57" title="The more a plan is practiced and stakeholders are trained on the plan, the more effectively they will be able to act before, during, and after an emergency to lessen the impact on life and property. Exercises provide chances to practice with community partners, and to identify gaps and weaknesses in your plan. The exercises listed here require increasing amounts of planning, time, and resources. Ideally, IHEs will create an exercise program, building from a tabletop exercise up to a more advanced exercise, like a functional exercise. For more information on exercises, see pp. 29-30 in the <i>IHE Guide</i>, or visit <a href='http://rems.ed.gov/IHEPPStep06.aspx' target='_blank'>http://rems.ed.gov/IHEPPStep06.aspx</a>."> executing any of these drills or exercises</a>. Some types of drills are required by states or localities.
</div>

<div class = "testtable">

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<a id="popupid58" title="Tabletop exercises are small-group discussions that walk through a scenario and the courses of action an IHE will need to take before, during, and after an emergency to lessen the impact on the IHE community." >Tabletop exercises</a>
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm19Texercises" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes, plan includes</asp:ListItem>
<asp:ListItem>No, plan doesn’t include</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator105" runat="server"  ControlToValidate="rblstIHEnm19Texercises" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8"/>
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<a id="popupid59" title="During drills, IHE personnel and community partners use the actual campus buildings, facilities, and grounds to practice responding to a scenario." >Drills</a>
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm19Drills" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes, plan includes</asp:ListItem>
<asp:ListItem>No, plan doesn’t include</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator106" runat="server"  ControlToValidate="rblstIHEnm19Drills" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<a id="popupid60" title="Functional exercises are like drills, but involve multiple partners. Participants react to realistic simulated events (e.g., a bomb threat), and implement the plan and procedures using the Incident Command System." >Functional exercises</a>
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm19Fexercises" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes, plan includes</asp:ListItem>
<asp:ListItem>No, plan doesn’t include</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator107" runat="server"  ControlToValidate="rblstIHEnm19Fexercises" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
<a id="popupid61" title="These exercises are the most time-consuming activity in the exercise continuum and are multiagency, multijurisdictional efforts in which all resources are deployed." >Full-scale exercises</a>
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm19FSexercises" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes, plan includes</asp:ListItem>
<asp:ListItem>No, plan doesn’t include</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator108" runat="server"  ControlToValidate="rblstIHEnm19FSexercises" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip57" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid57" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="350" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip58" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid58" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip59" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid59" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip60" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid60" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip61" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid61" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="300" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

</div>
</section>


<section>
<div class="question">
20.	When the plan is exercised, the planning team should evaluate the exercise to gain information and feedback on the success of the exercise (and the plan). 
    Does your EOP incorporate the following criteria to evaluate the plan when exercised?
</div>
<div class = "testtable">

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
The exercise included community partners.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm20partners" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator109" runat="server"  ControlToValidate="rblstIHEnm20partners" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>

<%--<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
The exercise included additional stakeholders, such as community organizations.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm20IHEOrganization" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server"  ControlToValidate="rblstIHEnm20IHEOrganization" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>--%>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
The planning team communicated information about the exercise in advance to avoid confusion and concern.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm20concern" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator110" runat="server"  ControlToValidate="rblstIHEnm20concern" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
The plan was exercised under different and non-ideal conditions (e.g., times of day, weather conditions, points in the academic calendar, absence of key personnel, and various IHE events).
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm20events" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator111" runat="server"  ControlToValidate="rblstIHEnm20events" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Consistent, common emergency management terminology was used.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm20used" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator112" runat="server"  ControlToValidate="rblstIHEnm20used" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Following the exercise, the planning team debriefed and developed an after-action report that evaluated results, identified gaps or shortfalls, and documented lessons learned.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm20learned" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator113" runat="server"  ControlToValidate="rblstIHEnm20learned" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
The planning team discussed how the higher ed EOP and procedures will be modified, if needed, and specified who has the responsibility for modifying the plan.
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm20theplan" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator114" runat="server"  ControlToValidate="rblstIHEnm20theplan" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>
</div>
</section>


<div class="intro-note">
Finally, a plan must be reviewed, revised, and maintained. 
</div>

<section>
<div class="question">
21.	Does your planning team regularly review and revise your plan following these incidents and events? 
</div>

<div class = "testtable">

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Actual emergencies
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm21emergencies" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator115" runat="server"  ControlToValidate="rblstIHEnm21emergencies" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Changes in policy, personnel, organizational structures, processes, facilities, or equipment
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm21equipment" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator124" runat="server"  ControlToValidate="rblstIHEnm21equipment" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>


<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Finalized formal updates of planning guidance or standards
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm21standards" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator116" runat="server"  ControlToValidate="rblstIHEnm21standards" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Formal exercises
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm21exercises" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator117" runat="server"  ControlToValidate="rblstIHEnm21exercises" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
Changes in the IHE and surrounding community
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm21community" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator118" runat="server"  ControlToValidate="rblstIHEnm21community" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8"/>
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
New or changed threats or hazards
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm21hazards" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator119" runat="server"  ControlToValidate="rblstIHEnm21hazards" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>

<fieldset>
<div class="questionBlock">
<div class="col-lg-6 col-xs-12">
New information generated from ongoing assessments
</div>                
<div class="col-lg-4 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm21assessments" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator120" runat="server"  ControlToValidate="rblstIHEnm21assessments" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>
</div>
</section>



<div class="intro-note">
Planning is a continuous process, even after a plan is approved and published. Plans should evolve as the IHE and planning team learn lessons, obtain new information and insights, and update priorities. 
</div>
        
<section>
<div class="question">
22.	Does your planning team have a process and schedule for reviewing and revising your plan? 
</div>

<div class = "testtable">
<fieldset>
<div class="questionBlock">
<div class="col-lg-10 col-xs-12">
<asp:RadioButtonList ID="rblstIHEnm22question" runat="server" 
RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem>Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
<asp:ListItem>I don’t know</asp:ListItem>
</asp:RadioButtonList>
</div>
<div class="col-lg-2 col-xs-12">
<asp:RequiredFieldValidator ID="RequiredFieldValidator121" runat="server"  ControlToValidate="rblstIHEnm22question" ForeColor="Red" SetFocusOnError="true"
ErrorMessage="*Required" ValidationGroup="vgView8" />
</div>
</div>
</fieldset>

</div>
</section> 
  
        

        
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button33" runat="server" Text="Previous"  OnClick="IHEPrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button17" runat="server" Text="Next" OnClick="IHENextClick" ValidationGroup="vgView8" />

            </div>
        </div>
        </asp:View>
        <asp:View ID="view20" runat="server" >
        <h2>Conclusion</h2>
         <div class="final-message">
        Thank you for completing EOP EVALUATE for IHEs! To access your customized results report, select Download.
       </div>
        
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="btnIHEBack" runat="server" Text="Previous"  OnClick="IHEPrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="lbtnIHEPrintReport" runat="server" Text="Download" OnClick="PrintReport" ValidationGroup="vgView8" />
            </div>
        </div>
        </asp:View>
    </asp:MultiView>
    <asp:HiddenField ID="hfTokenID" runat="server" />

        <asp:Panel ID="Panel1" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Select Evaluation Tool
                 <span class="closeit" style="padding-left:300px;"><asp:LinkButton ID="lbtnClose" Visible="false" runat="server" Text="X" OnClick="ClosePan"  CausesValidation="false" CssClass="closeit_link" />    
                </span>
                </div>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                    <div></div>
                    <div>
                        
                    </div>
                        <table width="100%">
                            <tr>
                                <td >
                                 I want to evaluate my:
                                </td>
                                </tr>
                                <tr>
                                <td >
                                  <asp:DropDownList ID="ddlTools" 
                                   AutoPostBack="true" OnTextChanged="ddlToolSelected" runat="server" ValidationGroup="popupgrp">
                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                    <asp:ListItem Value="1">School EOP</asp:ListItem>
                                    <%--<asp:ListItem Value="2">School District EOP</asp:ListItem>--%>
                                    <asp:ListItem Value="3">Higher ed EOP</asp:ListItem>
                                   </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvpopup" runat="server" ForeColor="Red" ControlToValidate="ddlTools" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnEOPOK" runat="server" Text="OK" ValidationGroup="popupgrp"
                                        OnClick="OnSelectTool" />&nbsp;&nbsp;
                                        <asp:Button ID="btnEOPCanel" runat="server" style="display:none;" ValidationGroup="popupgrp" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnEOPOK" />
                        <asp:AsyncPostBackTrigger ControlID="btnEOPCanel" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton3" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="mpeEOP" runat="server" TargetControlID="LinkButton3"
            PopupControlID="Panel1" DropShadow="true" OkControlID="btnEOPCanel" CancelControlID="btnEOPCanel"
            BackgroundCssClass="magnetMPE" Y="400" />

    </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Contentfooter" Runat="Server">
Page <asp:Literal ID="ltlpagepos" Text="1" runat="server" /> of 8
</asp:Content>

