﻿<%@ Page Title="" Language="C#" MasterPageFile="EOPEvals.master" AutoEventWireup="true" CodeFile="EOPDistrict.aspx.cs" Inherits="EOPDistrict" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheader" Runat="Server">
        <!--BEGIN NAVIGATION STEPS-->
        <div class="eval-navigation">
            	<div class="steps" id="district" style="display:inline;">
                	<ul>
                    <li class="stepNav" id="nav1" runat="server"><span id="Welcome"  runat="server">Welcome</span></li>
                    <li class="stepNav" id="nav2" runat="server"><span id="plan" runat="server">District Plan</span></li>
                    <li class="stepNav" id="nav3" runat="server"><span  id="emergency" runat="server">District-wide Emergency Planning Activities</span></li>
                    <li class="stepNav" id="nav4" runat="server"><span id="wide_emergencies" runat="server">District-wide Emergencies</span></li>
                    <li class="stepNav" id="nav5" runat="server"><span id="school" runat="server">Individual School Plans</span></li>
                    <li class="stepNav" id="nav6" runat="server"><span id="Conclusion" runat="server">Conclusion</span></li>
                    </ul>
                </div>                      
        </div>
        <!--END NAVIGATION STEPS-->
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="container"> 
    <div class="eval-content">
    <asp:MultiView ID="DistrictEOP" runat="server" ActiveViewIndex="0" >
    <asp:View ID="View1" runat="server">
<h1>For School Districts</h1>
<p>Welcome to EOP EVALUATE! This tool is designed to help you evaluate an existing school district emergency operations plan (EOP) against the guidelines set forth in the Federal guidance. EOP EVALUATE for School Districts is set up to help you determine the extent to which your existing plan aligns with the recommendations made by the U.S. Departments of Education, Homeland Security, Justice, and Health and Human Services, specifically, the planning principles, six-step planning process for plan development, and the structure of your plan, as set forth in the Federal guidance. The process of completing this tool will take you through the Federal guidance itself, and reading and responding to the prompts provided will help illustrate the concepts and recommendations provided. You may use this tool to help identify areas for plan enhancement, recognize strengths in your existing plan, and/or to learn more about the recommendations set forth in the Federal guidance. Please note that completion of this tool requires some familiarity with the guidance and the terminology used therein, and assumes the user possesses extensive knowledge of the content of their school district EOP. For more information on the Federal guidance, please visit the Readiness and Emergency Management for Schools (REMS) Technical Assistance (TA) Center’s Website at <a href="http://rems.ed.gov">http://rems.ed.gov</a>.
</p><p>Upon completing EOP EVALUATE for School Districts, your responses will result in a customized downloadable report containing a variety of additional resources and information relevant to your needs. This tool should take approximately 18-25 minutes to complete. Please note that no information is recorded during this process; your responses are used only to generate your customized report, and no data remains in our system upon completion and download of the report.
</p><p>For assistance using this tool, please contact the REMS TA Center Help Desk by email (<a href='mailto:info@remstacenter.org'>info@remstacenter.org</a>) or by telephone (855-781-7367 [REMS]) toll-free Monday through Friday between the hours of 9:00 a.m. and 5:00 p.m. Eastern Time.
</p><p>Let’s get started! Please note that all fields are required. Click NEXT to continue. </p>

        <!-- BEGIN PREV-NEXT NAVIGATION -->
        <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button14" runat="server" Text="Previous" OnClick="GoHome" />&nbsp;
        <asp:Button ID="Button15" runat="server" Text="Next" OnClick="NextClick" ValidationGroup="vgrpView2" />
            </div>
        </div>

        <!-- END PREV-NEXT NAVIGATION -->
</asp:View>
    <asp:View ID="View2" runat="server">
<h1>Demographic Information</h1>
               
                <p>
     <span >What type of organization are you from? <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqtypeOrg" runat="server" 
                  ValidationGroup="vgrpView2" ControlToValidate="ddl21"
                 ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator></span>
         <asp:DropdownList ID="ddl21" runat="server" ValidationGroup="vgrpView2">
         <asp:ListItem>LEA</asp:ListItem>
         <asp:ListItem>SEA</asp:ListItem>
         <asp:ListItem>K-12 School</asp:ListItem>
         <asp:ListItem>Other</asp:ListItem>
         </asp:DropdownList>
         </p>



     <p>
     <span>What is your role within your organization?<asp:RequiredFieldValidator SetFocusOnError="true" ID="reqRoleOrg" runat="server" 
                  ValidationGroup="vgrpView2" ControlToValidate="ddl22"
                 ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator></span>
        <asp:DropdownList ID="ddl22" runat="server" ValidationGroup="vgrpView2">
        <asp:ListItem>Superintendent</asp:ListItem>
<asp:ListItem>District Emergency Management</asp:ListItem>
<asp:ListItem>School Emergency Management</asp:ListItem>
<asp:ListItem>Law Enforcement Officer</asp:ListItem>
<asp:ListItem>School Resource Officer</asp:ListItem>
<asp:ListItem>Principal</asp:ListItem>
<asp:ListItem>Teacher</asp:ListItem>
<asp:ListItem>Administrator</asp:ListItem>
<asp:ListItem>Business Office</asp:ListItem>
<asp:ListItem>Central Administration or Designee</asp:ListItem>
<asp:ListItem>Support Personnel (Instructional Aide)</asp:ListItem>
<asp:ListItem>Counselor</asp:ListItem>
<asp:ListItem>Social Worker</asp:ListItem>
<asp:ListItem>Psychologist</asp:ListItem>
<asp:ListItem>Nurse</asp:ListItem>
<asp:ListItem>Maintenance Staff</asp:ListItem>
<asp:ListItem>Food Service or Cafeteria Worker</asp:ListItem>
<asp:ListItem>Bus Driver</asp:ListItem>
<asp:ListItem>Environmental Health and Safety</asp:ListItem>
<asp:ListItem>Facilities and Operations</asp:ListItem>
<asp:ListItem>Human Resources</asp:ListItem>
</asp:DropdownList>
</p>

                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button1" runat="server" Text="Previous"  OnClick="PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button2" runat="server" Text="Next" OnClick="NextClick" ValidationGroup="vgrpView2" />
            </div>
        </div>

    </asp:View>
    <asp:View ID="View3" runat="server">
    
 <section>
 <div class="question">1. Does your <a id="popupid1" title="A local education (LEA) or similar entity.">district</a> have an all-hazards <a id="popupid2" title="A local education agency (LEA) or school district EOP.">district plan</a>?
 </div>
 
 <div class = "testtable">
     <fieldset>
     	<div class="col-lg-12 col-xs-12">
             <asp:RadioButtonList
             ID="rbl311"
             runat="server"
             AutoPostBack="true"
             ValidationGroup="vgrpView3" 
             onselectedindexchanged="rbl311_SelectedIndexChanged">
             <asp:ListItem >Yes</asp:ListItem>
             <asp:ListItem>No</asp:ListItem>
             </asp:RadioButtonList>               
        </div>
        <div class="col-lg-12 col-xs-12">
             <asp:RequiredFieldValidator
             SetFocusOnError="true"
             ID="RequiredFieldValidator1"
             runat="server" 
             ValidationGroup="vgrpView3"
             ControlToValidate="rbl311"
             ForeColor="Red"
             Font-Size="Medium">Required</asp:RequiredFieldValidator> 
         </div>
     </fieldset>
 </div>
 </section> 
 
 <section>
 <div class="question">
 2. Did the process used to develop your plan include participants from the following? 
</div>
<div class="testtable">
         <fieldset ID="districtplan1" runat="server">
            <div class="questionBlock row">
                <div class="col-lg-6 col-xs-12"> District Administrators?</div>
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl321"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Not At All</asp:ListItem>
                        <asp:ListItem>Small Degree</asp:ListItem>
		   <asp:ListItem>Moderate Degree</asp:ListItem>
		    <asp:ListItem>High Degree</asp:ListItem>
                         </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="rqfdvalidator1"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl321"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
                
            <div class="questionBlock row">
                <div class="col-lg-6 col-xs-12">Building Level Administrators?</div>
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl322"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Not At All</asp:ListItem>
                        <asp:ListItem>Small Degree</asp:ListItem>
		   				<asp:ListItem>Moderate Degree</asp:ListItem>
		    			<asp:ListItem>High Degree</asp:ListItem>
                     </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="rqfdvalidator2"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl322"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
                
            <div class="questionBlock row">
                <div class="col-lg-6 col-xs-12">Instructional and Support Staff (e.g., teachers, school nurses)?</div>
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl323"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Not At All</asp:ListItem>
                        <asp:ListItem>Small Degree</asp:ListItem>
		   				<asp:ListItem>Moderate Degree</asp:ListItem>
		   				<asp:ListItem>High Degree</asp:ListItem>
                      </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator3"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl323"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
                
            <div class="questionBlock row">
                <div class="col-lg-6 col-xs-12">Food, Maintenance, Building or Grounds Staff (e.g., cafeteria workers, custodians)?</div>
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl324"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Not At All</asp:ListItem>
                        <asp:ListItem>Small Degree</asp:ListItem>
		   <asp:ListItem>Moderate Degree</asp:ListItem>
		    <asp:ListItem>High Degree</asp:ListItem>
                         </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator4"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl324"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
                
            <div class="questionBlock row">
                <div class="col-lg-6 col-xs-12"><a id="popupid3" title="Those with a <u>responsibility</u> in school emergency management, including local government, first responders (law enforcement officers, fire officials, and emergency medical services personnel), as well as public and mental health entities.">Community Partners</a>?</div>
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl325"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Not At All</asp:ListItem>
                        <asp:ListItem>Small Degree</asp:ListItem>
		   <asp:ListItem>Moderate Degree</asp:ListItem>
		    <asp:ListItem>High Degree</asp:ListItem>
                         </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator5"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl325"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>

           <div class="questionBlock row">
                <div class="col-lg-6 col-xs-12">     <a id="popupid4" title="Those entities within the community that can <u>support</u> emergency management for schools (e.g., Red Cross, Boys & Girls Club, faith-based organizations).">Community Organizations</a>?</div>
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl326"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Not At All</asp:ListItem>
                        <asp:ListItem>Small Degree</asp:ListItem>
		   <asp:ListItem>Moderate Degree</asp:ListItem>
		    <asp:ListItem>High Degree</asp:ListItem>
                         </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator6"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl326"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
                
            <div class="questionBlock row">
                <div class="col-lg-6 col-xs-12">Parents and Guardians (i.e., adults with authorized custody over a student)?</div>
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl327"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Not At All</asp:ListItem>
                        <asp:ListItem>Small Degree</asp:ListItem>
		   <asp:ListItem>Moderate Degree</asp:ListItem>
		    <asp:ListItem>High Degree</asp:ListItem>
                         </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator7"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl327"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
                
        </fieldset> 

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip3" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid3" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip4" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid4" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>

</div> 
 </section>
 
<section>
<div class="question">3. Did your district conduct a risk and vulnerability assessment?</div>
<div class="testtable">

        <fieldset ID="districtplan2" runat="server">
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl331"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                         </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator8"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl331"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
        </fieldset> 


</div>
</section>

<section>
<div class="question">
4. Does your plan align with county, local, or regional plans?
</div>
<div class="testtable">
         <fieldset ID="districtplan3" runat="server">
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">Parents and Guardians (i.e., adults with authorized custody over a student)?</div>
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl341"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                         </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator9"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl341"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
        </fieldset> 

</div>

</section>
<section>
<div class="question">5. Have all or parts of your plan been shared with district leadership for official approval?</div>
<div class="testtable">

        <fieldset ID="districtplan4" runat="server">
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl351"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                         </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator10"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl351"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
        </fieldset> 

</div>
</section>

<section>
<div class="question">6. Have all or parts of your plan been shared with community partners?</div>
<div class="testtable">

        <fieldset ID="districtplan5" runat="server">
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl361"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                         </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator11"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl361"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
        </fieldset> 
</div>
</section>

 <section>
<div class="question">7. Does your district plan include a process for reviewing and updating your district plan?</div>
<div class="testtable">

        <fieldset ID="districtplan6" runat="server">
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl371"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                         </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator12"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl371"
                        ForeColor="Red"
                        SetFocusOnError="true"
                        Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
        </fieldset> 
</div>
</section>    
   
 <section>
<div class="question">8. Has your district plan been reviewed and updated in the last three (3) years?</div>
<div class="testtable">

        <fieldset ID="districtplan7" runat="server">
            <div class="questionBlock">
                <div class="col-lg-6 col-xs-12">
                    <asp:RadioButtonList
                    ID="rbl381"
                    runat="server"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                         </asp:RadioButtonList>                
                </div>
                <div class="col-lg-12 col-xs-12">
                        <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator13"
                        runat="server"
                        ValidationGroup="vgrpView3"
                        ControlToValidate="rbl381"
                        ForeColor="Red"
                        SetFocusOnError="true"
						Font-Size="Medium">Required</asp:RequiredFieldValidator> 
                 </div>
                </div>
        </fieldset> 
</div>
</section> 
     

     
                <!-- BEGIN PREV-NEXT NAVIGATION -->
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button3" runat="server" Text="Previous"  OnClick="PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button4" runat="server" Text="Next" OnClick="NextClick" ValidationGroup="vgrpView3" />
            </div>
        </div>
    </asp:View>
    <asp:View ID="View4" runat="server">
    <fieldset>
    <div class="questionBlock">
    <div >District-wide Emergency Planning Activities</div>
    <p>This section asks about the parts of your plan that provide for district-wide emergency management planning.
    </p>
    <p>9. Does your district plan address the following district-wide activities:
    </p>
    <telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip5" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid5" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
    <p>Accounting for the <a id="popupid5" title="The needs of individuals with disabilities, as well as communication, language, medical, or transportation needs.">access and functional needs</a> of all individuals?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator14" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl491"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
     <asp:RadioButtonList ID="rbl491" runat="server" ValidationGroup="vgrpView4">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
        </div>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip6" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid6" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
   <div class="questionBlock">
    <p>Establishing <a id="popupid6" title="Active working relationships between entities governed by Memoranda of Understanding (MOUs) or Memoranda of Agreement (MOAs).">formal agreements</a> with community partners and/or community organizations?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator18" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl492"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
          <asp:RadioButtonList ID="rbl492" runat="server" ValidationGroup="vgrpView4">
             <asp:ListItem>No</asp:ListItem>
             <asp:ListItem>Yes</asp:ListItem>
             </asp:RadioButtonList>
  </div>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip7" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid7" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
    <div class="questionBlock">
    <p>Establishing <a id="popupid7" title="Active working relationships between entities not bound by a MOU, MOA, state law, or other type of formal agreement.">informal agreements</a> with community partners and/or community organizations?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator19" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl493"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
          <asp:RadioButtonList ID="rbl493" runat="server" ValidationGroup="vgrpView4">
             <asp:ListItem>No</asp:ListItem>
             <asp:ListItem>Yes</asp:ListItem>
             </asp:RadioButtonList>
    </div>
    <div class="questionBlock">
    <p>Obtaining and maintaining emergency equipment and supplies?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator20" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl494"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
         <asp:RadioButtonList ID="rbl494" runat="server" ValidationGroup="vgrpView4">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip8" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid8" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
    <div class="questionBlock">
    <p>Use of the <a id="popupid8" title="A standardized approach for incident management, regardless of cause, size, location, or complexity.">Incident Command System (ICS)</a>?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator21" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl495"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
          <asp:RadioButtonList ID="rbl495" runat="server" ValidationGroup="vgrpView4">
             <asp:ListItem>No</asp:ListItem>
             <asp:ListItem>Yes</asp:ListItem>
             </asp:RadioButtonList>
  </div>
   <div class="questionBlock">
    <p>
    10. Does your district plan include the following key overarching components, including:
    </p>
    <telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip9" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid9" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip10" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid10" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip11" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid11" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip12" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid12" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip13" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid13" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip14" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid14" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
    <p>
    Policies that address the <a id="popupid9" title=" The capabilities necessary, and the actions taken, to avoid, deter, or stop an imminent crime or threated or actual mass casualty incident.">prevention</a> and <a id="popupid10" title=" The capabilities necessary to eliminate or reduce the loss of life and property damage by lessening the impact of an event or emergency; and, reducing the likelihood that threats and hazards will happen.">mitigation</a> of, 
    <a id="popupid11" title="The capabilities, and ongoing actions, to secure districts and schools (including students, teachers, staff, visitors, networks, and property) against acts of violence and manmade or natural disasters.">protection</a> from, <a id="popupid12" title="The capabilities necessary to stabilize an emergency once it has already happened or is certain to happen in an unpreventable way; establish a safe and secure environment; save lives and property; and facilitate the transition to recovery.">response</a> to, and 
    <a id="popupid13" title="The capabilities necessary to assist schools within the district (and the district overall) affected by an event or emergency in restoring the learning environment.">recovery</a> from <a id="popupid14" title="Threats are human-caused emergencies such as crime and violence, while hazards include natural disasters, disease outbreaks, and accidents.">threats or hazards</a>?
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator22" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl4101"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList  ID="rbl4101" runat="server" ValidationGroup="vgrpView4">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
   <div class="questionBlock">
    <p>
    Continuity of Operations (COOP) that provides for the continuity of essential services (e.g., business services, communication, computer and systems support, facilities maintenance, safety and security, transportation, and continuity of teaching and learning) during an emergency and its immediate aftermath up to 30 days? 
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator23" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl4102"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl4102" runat="server" ValidationGroup="vgrpView4">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
   </div>
    <div class="questionBlock">
 <p>
    Organization and Assignment of Responsibilities (i.e., an overview of the broad roles and responsibilities of administration, staff, support personnel, community partners, parents and guardians, and of organizational functions during all emergencies)?
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator24" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl4103"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl4103" runat="server" ValidationGroup="vgrpView4">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
 </div>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip15" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid15" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
    <div class="questionBlock">
 <p>
    Direction, Control, and Coordination (i.e., the framework for all direction, control, and coordination to explain the 
    <a id="popupid15" title="A standardized approach for incident management, regardless of cause, size, location, or complexity.">ICS</a> structure as used by the district; the relationship between the district and the broader community’s emergency management system; and who maintains control of the equipment, resources, and supplies needed to support the district plan)?
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator25" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl4104"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl4104" runat="server" ValidationGroup="vgrpView4">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
 </div>
     <div class="questionBlock">
 <p>
    Information Collection, Analysis, and Dissemination (i.e., identifying the type and role of information in the successful implementation of the activities that occur before, during, and after an emergency)?
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator26" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl4105"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl4105" runat="server" ValidationGroup="vgrpView4">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
 </div>
     <div class="questionBlock">
<p>
    Administration, Finance, and Logistics (i.e., general support requirements and the availability of services and support for all types of emergencies, as well as general policies for managing resources)?
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator27" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl4106"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl4106" runat="server" ValidationGroup="vgrpView4">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div class="questionBlock">
<p>
    Training and Exercises (i.e., the training and exercise activities the district or school use in support of the plan, such as tabletop exercises, drills, functional exercises, and/or full-scale exercises)?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator28" runat="server" 
        ValidationGroup="vgrpView4" ControlToValidate="rbl4107"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl4107" runat="server" ValidationGroup="vgrpView4">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
 </div>

    </fieldset>
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button11" runat="server" Text="Previous"  OnClick="PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button12" runat="server" Text="Next" OnClick="NextClick" ValidationGroup="vgrpView4" />
            </div>
        </div>

    </asp:View>
    <asp:View ID="View5" runat="server">
     <fieldset>
    <div class="questionBlock">
    <div >District-wide Emergencies</div>
    <p>This section relates to the elements of your plan that pertain to district-wide emergencies, and plans for protecting all of the schools in the district.
    </p>
    <telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip16" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid16" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
    <p>
    11. To what degree does your district plan’s annexes include <a id="popupid16" title="The what, who, when, where, why, and how for each threat, hazard, and function within an EOP.">courses of action</a> to do the following:
    </p>
    <p>Prevent district-wide emergencies?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator29" runat="server" 
        ValidationGroup="vgrpView5" ControlToValidate="rbl5111"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
            <asp:RadioButtonList ID="rbl5111" runat="server" ValidationGroup="vgrpView5">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div class="questionBlock">
    <p>Protect all of the schools in the district?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator15" runat="server" 
        ValidationGroup="vgrpView5" ControlToValidate="rbl5112"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl5112" runat="server" ValidationGroup="vgrpView5">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
     <div class="questionBlock">
    <p>Mitigate a district-wide emergency?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator30" runat="server" 
        ValidationGroup="vgrpView5" ControlToValidate="rbl5113"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl5113" runat="server" ValidationGroup="vgrpView5">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div class="questionBlock">
    <p>Respond to a district-wide emergency?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator31" runat="server" 
        ValidationGroup="vgrpView5" ControlToValidate="rbl5114"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList  ID="rbl5114" runat="server" ValidationGroup="vgrpView5">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div class="questionBlock">
    <p>Recover from a district-wide emergency?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator32" runat="server" 
        ValidationGroup="vgrpView5" ControlToValidate="rbl5115"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl5115" runat="server" ValidationGroup="vgrpView5">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    </fieldset>
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button5" runat="server" Text="Previous"  OnClick="PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button6" runat="server" Text="Next" OnClick="NextClick" ValidationGroup="vgrpView5" />
            </div>
        </div>
    </asp:View>
    <asp:View ID="View6" runat="server">
     <fieldset>
    <div class="questionBlock">
    <div>Development of Individual School Plans</div>
    <p>
    This section asks about the extent of guidance and support your district plan provides to schools for school plan development.
    </p>
    <telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip17" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid17" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
 <telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip18" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid18" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
    <p>
    12. Does your district plan provide a process for <a id="popupid17" title="A school building or campus.">schools</a> to use to develop 
    their own <a id="popupid18" title="A school building or campus EOP.">school plans</a>?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator16" runat="server" 
        ValidationGroup="vgrpView6" ControlToValidate="rbl6121"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl6121" runat="server" AutoPostBack="true" 
            ValidationGroup="vgrpView6" 
            onselectedindexchanged="rbl6121_SelectedIndexChanged">
        <asp:ListItem>No</asp:ListItem>
        <asp:ListItem>Yes</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div id="schoolplan1" runat="server" class="questionBlock">
<p>
13. Does the process include
</p>
    <p>Participation of district personnel in the process to help schools develop their school plan?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator33" runat="server" 
        ValidationGroup="vgrpView6" ControlToValidate="rbl6131"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl6131" runat="server" ValidationGroup="vgrpView6">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div id="schoolplan2" runat="server" class="questionBlock">
    <p>Guidance for schools on conducting site assessments (i.e., an assessment that examines the safety, accessibility, and emergency preparedness of the school’s buildings and grounds)?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator34" runat="server" 
        ValidationGroup="vgrpView6" ControlToValidate="rbl6132"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl6132" runat="server" ValidationGroup="vgrpView6">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div id="schoolplan3" runat="server" class="questionBlock">
    <p>Guidance on conducting capacity assessments (i.e., an assessment that determines what resources are available, such as the capabilities of students and staff, as well as the services and material resources of community partners)?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator35" runat="server" 
        ValidationGroup="vgrpView6" ControlToValidate="rbl6133"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl6133" runat="server" ValidationGroup="vgrpView6">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div id="schoolplan4" runat="server" class="questionBlock">
    <p>Guidance on conducting culture and climate assessments (i.e., an assessment that evaluates student and staff connectedness to the school and problem behaviors)?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator36" runat="server" 
        ValidationGroup="vgrpView6" ControlToValidate="rbl6134"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl6134" runat="server" ValidationGroup="vgrpView6">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div id="schoolplan5" runat="server" class="questionBlock">
    <p>Guidance on conducting behavioral threat assessments (i.e., an assessment that analyzes communication and behaviors to determine whether or not a student, staff, or other person may pose a threat)?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator37" runat="server" 
        ValidationGroup="vgrpView6" ControlToValidate="rbl6135"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl6135" runat="server" ValidationGroup="vgrpView6">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div id="schoolplan6" runat="server" class="questionBlock">
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip19" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid19" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
    <p>Guidance on how to prioritize <a id="popupid19" title="Broad, general statements that indicate a desired outcome in response to the threat or hazard identified within an EOP.">threats and hazards</a> identified through assessment?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator38" runat="server" 
        ValidationGroup="vgrpView6" ControlToValidate="rbl6136"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
    </p>
        <asp:RadioButtonList ID="rbl6136" runat="server" ValidationGroup="vgrpView6">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div id="schoolplan7" runat="server" class="questionBlock">
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip20" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid20" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip21" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid21" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip22" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid22" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<p>
14. Does your district plan include guidance for schools on how to develop their own <a id="popupid20" title="Broad, general statements that indicate a desired outcome in response to the threat or hazard identified within an EOP.">goals</a> ,
<a id="popupid21" title="Specific, measurable actions that are necessary to achieve the goal(s) identified within an EOP.">objectives</a> , 
and <a id="popupid22" title="The what, who, when, where, why, and how for each threat, hazard, and function within an EOP.">courses of action</a> using the “before, during, and after” approach (i.e., the three time frames associated with an incident)?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator39" runat="server" 
        ValidationGroup="vgrpView6" ControlToValidate="rbl6141"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
</p>
        <asp:RadioButtonList ID="rbl6141" runat="server" ValidationGroup="vgrpView6">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div id="schoolplan8" runat="server" class="questionBlock">
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip23" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid23" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip24" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid24" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<telerik:RadToolTip Skin="Sunset" runat="server" ID="RadToolTip25" HideEvent="ManualClose"
ShowEvent="OnClick" RelativeTo="Element" TargetControlID="popupid25" IsClientID="true"
Animation="Fade" ManualCloseButtonText="Close" Position="MiddleRight" ContentScrolling="Default"
Width="200" Height="32" RenderInPageRoot="true">
</telerik:RadToolTip>
<p>
15. Does your district plan include guidance for schools on formatting the plan to include a 
<a id="popupid23" title="The section of the EOP that provides an overview of the district’s or school’s approach to operations before, during, and after an emergency. This section addresses the overarching activities the district or school will undertake regardless of the function, threat, or hazard.">basic plan</a> , <a id="popupid24" title="The section of an EOP that details the goals, objectives, and courses of action related to functions (e.g., evacuation, communications, recovery) that apply across multiple threats and/or hazards. These set forth how the district or school will manage a function before, during, and after an emergency.">functional annexes</a> , 
and threat- and <a id="popupid25" title="The section of an EOP that specifies the goals, objectives, and courses of action that a district or school will follow to address a particular type of threat or hazard (e.g., hurricane, active shooter). These set forth how the district or school will manage a threat or hazard before, during, and after an emergency.">hazard-specific annexes</a>?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator40" runat="server" 
        ValidationGroup="vgrpView6" ControlToValidate="rbl6151"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
</p>
        <asp:RadioButtonList ID="rbl6151" runat="server" ValidationGroup="vgrpView6">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div id="schoolplan9" runat="server" class="questionBlock">
<p>
16. Does your district plan include guidance for schools on plan implementation and maintenance, to include training stakeholders, exercising the plan, and updating and maintaining the plan?
        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator41" runat="server" 
        ValidationGroup="vgrpView6" ControlToValidate="rbl6161"
        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
</p>
        <asp:RadioButtonList ID="rbl6161" runat="server" ValidationGroup="vgrpView6">
        <asp:ListItem>Not At All</asp:ListItem>
        <asp:ListItem>Small Degree</asp:ListItem>
        <asp:ListItem>Moderate Degree</asp:ListItem>
        <asp:ListItem>High Degree</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    </fieldset>
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button7" runat="server" Text="Previous"  OnClick="PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button8" runat="server" Text="Next" OnClick="NextClick" ValidationGroup="vgrpView6" />
            </div>
        </div>
    </asp:View>
    <asp:View ID="View7" runat="server">
     <fieldset>
    <div class="questionBlock"><div>Conclusion</div>
    <p>Thank you for completing EOP AVALUATE! To access your customized results report, please select Next.</p>
    </div>
    </fieldset>
    <div class="eval-prev-next">
        	<div class="container1">
        <asp:Button ID="Button9" runat="server" Text="Previous"  OnClick="PrevClick" CausesValidation="false" />&nbsp;&nbsp;
        <asp:Button ID="Button10" runat="server" Text="Next" OnClick="PrintReport" />
            </div>
        </div>
    </asp:View>
</asp:MultiView>
</div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Contentfooter" Runat="Server">
    <asp:Literal ID="ltlPageNo" runat="server"></asp:Literal>
</asp:Content>



