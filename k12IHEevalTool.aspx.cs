﻿using System;
using System.Web;
using System.Data;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Linq;
using System.Web.UI.WebControls;

public partial class k12IHEevalTool : System.Web.UI.Page
{
    int initIndx = 1;
    
    rptDataClassesDataContext db = new rptDataClassesDataContext();
    private void initialFields(int idx)
    {
        if (ddlTools.SelectedValue=="1")
        {
            ddlk12orgrole.SelectedIndex = idx;
            ddlk12orgtype.SelectedIndex = idx;

            rblstk12isschcomleaderAffair.SelectedIndex = idx;
            rblstk12isschcomadminOffice.SelectedIndex = idx;
            rblstk12isschcomeduDesignee.SelectedIndex = idx;
            rblstk12isschcomDistrictService.SelectedIndex = idx;
            rblstk12isschcompsychSafety.SelectedIndex = idx;
            rblstk12isschcomconslorOperation.SelectedIndex = idx;
            rblstk12isschcomnurseService.SelectedIndex = idx;
            rblstk12isschcomfmanagerHservice.SelectedIndex = idx;
            rblstk12isschcomtmanagerHRes.SelectedIndex = idx;
            rblstk12isschcomfpersonITech.SelectedIndex = idx;
            rblstk12isschcomcstaffCousel.SelectedIndex = idx;
            rblstk12isschcomSROsOffice.SelectedIndex = idx;
            rblstk12isschcomITSpecOperation.SelectedIndex = idx;
            rblstk12isschcomfsrepLife.SelectedIndex = idx;
            rblstk12isschcomstudentAffair.SelectedIndex = idx;
            rblstk12isschcomfamilyTrans.SelectedIndex = idx;
            rblstk12ischcomASchrepOffice.SelectedIndex = idx;

            rblstk12isINTREPindiv.SelectedIndex = idx;
            rblstk12isINTREPracial.SelectedIndex = idx;
            rblstk12isINTREPReligious.SelectedIndex = idx;
            rblstk12isINTREPlimited.SelectedIndex = idx;
            rblstk12isComPart1stResp.SelectedIndex = idx;
            rblstk12isComPartlocalStaff.SelectedIndex = idx;
            rblstk12isComPartLawofficer.SelectedIndex = idx;
            rblstk12isComPartEMSPersonnel.SelectedIndex = idx;
            rblstk12isComPartSchResOfficer.SelectedIndex = idx;
            rblstk12isComPartFireOfficial.SelectedIndex = idx;
            rblstk12isComPartPHPract.SelectedIndex = idx;
            rblstk12isComPartMHPract.SelectedIndex = idx;
            rblstk12isAPRSEMPMLEofficial.SelectedIndex = idx;
            rblstk12isAPRSEMCBRorganize.SelectedIndex = idx;
            rblstk12isAPRSEMBusPart.SelectedIndex = idx;
            rblstk12isAPRSEMCBYOrganize.SelectedIndex = idx;
            rblstk12isAPRSEMSFLEHSOfficial.SelectedIndex = idx;
            rblstk12isAPRSEMDSAGeneral.SelectedIndex = idx;
            rblstk12isAPRSEMLSSDepart.SelectedIndex = idx;
            rblstk12isAPRSEMDisOrganize.SelectedIndex = idx;
            rblstk12isAPRSEMMedia.SelectedIndex = idx;
            rblstk12is2NIMS.SelectedIndex = idx;
            rblstk12is2UniRole.SelectedIndex = idx;
            rblstk12is2RegMeeting.SelectedIndex = idx;
            rblstk12nm3Highquality.SelectedIndex = idx;
            rblstk12nm4Highquality.SelectedIndex = idx;
            rblstk12nm5Highquality.SelectedIndex = idx;
            rblstk12nm6Highquality.SelectedIndex = idx;
            rblstk12nm7Highquality.SelectedIndex = idx;
            rblstk12nm8aHighquality.SelectedIndex = idx;
            rblstk12nm8bHighquality.SelectedIndex = idx;
            rblstk12nm9Highquality.SelectedIndex = idx;
            rblstk12is10taken.SelectedIndex = idx;
            rblstk12is10responsibleaction.SelectedIndex = idx;
            rblstk12is10takeplace.SelectedIndex = idx;
            rblstk12is10available.SelectedIndex = idx;
            rblstk12is10prioraction.SelectedIndex = idx;
            rblstk12is10afteraction.SelectedIndex = idx;
            rblstk12is10necessaryaction.SelectedIndex = idx;
            rblstk12is10needs.SelectedIndex = idx;
            rblstk12nm11Highquality.SelectedIndex = idx;
            rblstk12is12Evacuation.SelectedIndex = idx;
            rblstk12is12Lockdown.SelectedIndex = idx;
            rblstk12is12ShelterinPlace.SelectedIndex = idx;
            rblstk12is12AccountingAllPersons.SelectedIndex = idx;
            rblstk12is12ComWarning.SelectedIndex = idx;
            rblstk12is12ReunificationAssessment.SelectedIndex = idx;
            rblstk12is12COOP.SelectedIndex = idx;
            rblstk12is12Recovery.SelectedIndex = idx;
            rblstk12is12PHMMHealth.SelectedIndex = idx;
            rblstk12is12Security.SelectedIndex = idx;
            rblstk12is12onlist.SelectedIndex = idx;
            rblstk12is13NHazards.SelectedIndex = idx;
            rblstk12is13THazards.SelectedIndex = idx;
            rblstk12is13BHazards.SelectedIndex = idx;
            rblstk12is13Threats.SelectedIndex = idx;
            rblstk12nm14flowcharts.SelectedIndex = idx;
            rblstk12nm14activevoice.SelectedIndex = idx;
            rblstk12nm14theyneed.SelectedIndex = idx;
            rblstk12nm14actionable.SelectedIndex = idx;
            rblstk12nm14audioBraille.SelectedIndex = idx;
            rblstk12nm15effectively.SelectedIndex = idx;
            rblstk12nm15assignedfunction.SelectedIndex = idx;
            rblstk12nm15reasonable.SelectedIndex = idx;
            rblstk12nm15theplan.SelectedIndex = idx;
            rblstk12nm15threathazard.SelectedIndex = idx;
            rblstk12nm15limitations.SelectedIndex = idx;
            rblstk12nm15thelaw.SelectedIndex = idx;
            rblstk12nm15idfunctions.SelectedIndex = idx;
            rblstk12nm15schcommunity.SelectedIndex = idx;
            rblstk12nm15WhoseDirection.SelectedIndex = idx;
            rblstk12nm15utmostpriority.SelectedIndex = idx;
            rblstk12nm15endstate.SelectedIndex = idx;
            rblstk12nm15localrequirements.SelectedIndex = idx;
            rblstk12nm16leadership.SelectedIndex = idx;
            rblstk12nm16community.SelectedIndex = idx;
            rblstk12nm16items.SelectedIndex = idx;
            rblstk12nm17leadership.SelectedIndex = idx;
            rblstk12nm17partnersplan.SelectedIndex = idx;
            rblstk12nm17stakeholdersplan.SelectedIndex = idx;
            rblstk12nm17building.SelectedIndex = idx;
            rblstk12nm17recordsplan.SelectedIndex = idx;
            rblstk12nm17organizationsplan.SelectedIndex = idx;
            rblstk12nm18stakeholdersplan.SelectedIndex = idx;
            rblstk12nm18parties.SelectedIndex = idx;
            rblstk12nm18procedures.SelectedIndex = idx;
            rblstk12nm18needs.SelectedIndex = idx;
            rblstk12nm18partners.SelectedIndex = idx;
            rblstk12nm18roles.SelectedIndex = idx;
            rblstk12nm19Texercises.SelectedIndex = idx;
            rblstk12nm19Drills.SelectedIndex = idx;
            rblstk12nm19Fexercises.SelectedIndex = idx;
            rblstk12nm19FSexercises.SelectedIndex = idx;
            rblstk12nm20partners.SelectedIndex = idx;
            rblstk12nm20concern.SelectedIndex = idx;
            rblstk12nm20events.SelectedIndex = idx;
            rblstk12nm20used.SelectedIndex = idx;
            rblstk12nm20learned.SelectedIndex = idx;
            rblstk12nm20theplan.SelectedIndex = idx;
            rblstk12nm21emergencies.SelectedIndex = idx;
            rblstk12nm21equipment.SelectedIndex = idx;
            rblstk12nm21standards.SelectedIndex = idx;
            rblstk12nm21exercises.SelectedIndex = idx;
            rblstk12nm21community.SelectedIndex = idx;
            rblstk12nm21hazards.SelectedIndex = idx;
            rblstk12nm21assessments.SelectedIndex = idx;
            rblstk12nm22question.SelectedIndex = idx;
        }
        else if(ddlTools.SelectedValue=="3")
        {
            ddlIHEorgrole.SelectedIndex = idx;
            ddlIHEorgtype.SelectedIndex = idx;

            rblstIHEisschcomleaderAffair.SelectedIndex = idx;
            rblstIHEisschcomadminOffice.SelectedIndex = idx;
            rblstIHEisschcomeduDesignee.SelectedIndex = idx;
            rblstIHEisschcomDistrictService.SelectedIndex = idx;
            rblstIHEisschcompsychSafety.SelectedIndex = idx;
            rblstIHEisschcomconslorOperation.SelectedIndex = idx;
            rblstIHEisschcomnurseService.SelectedIndex = idx;
            rblstIHEisschcomfmanagerHservice.SelectedIndex = idx;
            rblstIHEisschcomtmanagerHRes.SelectedIndex = idx;
            rblstIHEisschcomfpersonITech.SelectedIndex = idx;
            rblstIHEisschcomcstaffCousel.SelectedIndex = idx;
            rblstIHEisschcomSROsOffice.SelectedIndex = idx;
            rblstIHEisschcomITSpecOperation.SelectedIndex = idx;
            rblstIHEisschcomfsrepLife.SelectedIndex = idx;
            rblstIHEisschcomstudentAffair.SelectedIndex = idx;
            rblstIHEisschcomfamilyTrans.SelectedIndex = idx;
            rblstIHEischcomASchrepOffice.SelectedIndex = idx;
            rblstIHEisschcomIHErep.SelectedIndex = idx;

            rblstIHEisINTREPindiv.SelectedIndex = idx;
            rblstIHEisINTREPracial.SelectedIndex = idx;
            rblstIHEisINTREPReligious.SelectedIndex = idx;
            rblstIHEisINTREPlimited.SelectedIndex = idx;
            rblstIHEisINTREPiheStudents.SelectedIndex = idx;

            rblstIHEisComPart1stResp.SelectedIndex = idx;
            rblstIHEisComPartlocalStaff.SelectedIndex = idx;
            rblstIHEisComPartLawofficer.SelectedIndex = idx;
            rblstIHEisComPartEMSPersonnel.SelectedIndex = idx;
            //rblstIHEisComPartSchResOfficer.SelectedIndex = idx;
            rblstIHEisComPartFireOfficial.SelectedIndex = idx;
            rblstIHEisComPartPHPract.SelectedIndex = idx;
            rblstIHEisComPartMHPract.SelectedIndex = idx;
            rblstIHEisAPRSEMPMLEofficial.SelectedIndex = idx;
            rblstIHEisAPRSEMCBRorganize.SelectedIndex = idx;
            rblstIHEisAPRSEMBusPart.SelectedIndex = idx;
            rblstIHEisAPRSEMCBYOrganize.SelectedIndex = idx;
            rblstIHEisAPRSEMSFLEHSOfficial.SelectedIndex = idx;
            rblstIHEisAPRSEMDSAGeneral.SelectedIndex = idx;
            rblstIHEisAPRSEMLSSDepart.SelectedIndex = idx;
            rblstIHEisAPRSEMDisOrganize.SelectedIndex = idx;
            rblstIHEisAPRSEMMedia.SelectedIndex = idx;
            rblstIHEis2NIMS.SelectedIndex = idx;
            rblstIHEis2UniRole.SelectedIndex = idx;
            rblstIHEis2RegMeeting.SelectedIndex = idx;
            rblstIHEnm3Highquality.SelectedIndex = idx;
            rblstIHEnm4Highquality.SelectedIndex = idx;
            rblstIHEnm5Highquality.SelectedIndex = idx;
            rblstIHEnm6Highquality.SelectedIndex = idx;
            rblstIHEnm7Highquality.SelectedIndex = idx;
            rblstIHEnm8aHighquality.SelectedIndex = idx;
            rblstIHEnm8bHighquality.SelectedIndex = idx;
            rblstIHEnm9Highquality.SelectedIndex = idx;
            rblstIHEis10taken.SelectedIndex = idx;
            rblstIHEis10responsibleaction.SelectedIndex = idx;
            rblstIHEis10takeplace.SelectedIndex = idx;
            rblstIHEis10available.SelectedIndex = idx;
            rblstIHEis10prioraction.SelectedIndex = idx;
            rblstIHEis10afteraction.SelectedIndex = idx;
            rblstIHEis10necessaryaction.SelectedIndex = idx;
            rblstIHEis10needs.SelectedIndex = idx;
            rblstIHEnm11Highquality.SelectedIndex = idx;
            rblstIHEis12Evacuation.SelectedIndex = idx;
            rblstIHEis12Lockdown.SelectedIndex = idx;
            rblstIHEis12ShelterinPlace.SelectedIndex = idx;
            rblstIHEis12AccountingAllPersons.SelectedIndex = idx;
            rblstIHEis12ComWarning.SelectedIndex = idx;
            rblstIHEis12ReunificationAssessment.SelectedIndex = idx;
            rblstIHEis12COOP.SelectedIndex = idx;
            rblstIHEis12Recovery.SelectedIndex = idx;
            rblstIHEis12PHMMHealth.SelectedIndex = idx;
            rblstIHEis12Security.SelectedIndex = idx;
            rblstIHEis12onlist.SelectedIndex = idx;
            rblstIHEis13NHazards.SelectedIndex = idx;
            rblstIHEis13THazards.SelectedIndex = idx;
            //rblstIHEis13BHazards.SelectedIndex = idx;
            rblstIHEis13Threats.SelectedIndex = idx;
            rblstIHEnm14flowcharts.SelectedIndex = idx;
            rblstIHEnm14activevoice.SelectedIndex = idx;
            rblstIHEnm14theyneed.SelectedIndex = idx;
            rblstIHEnm14actionable.SelectedIndex = idx;
            rblstIHEnm14audioBraille.SelectedIndex = idx;
            rblstIHEnm15effectively.SelectedIndex = idx;
            rblstIHEnm15assignedfunction.SelectedIndex = idx;
            rblstIHEnm15reasonable.SelectedIndex = idx;
            rblstIHEnm15theplan.SelectedIndex = idx;
            rblstIHEnm15threathazard.SelectedIndex = idx;
            rblstIHEnm15limitations.SelectedIndex = idx;
            rblstIHEnm15thelaw.SelectedIndex = idx;
            rblstIHEnm15idfunctions.SelectedIndex = idx;
            rblstIHEnm15schcommunity.SelectedIndex = idx;
            rblstIHEnm15WhoseDirection.SelectedIndex = idx;
            rblstIHEnm15utmostpriority.SelectedIndex = idx;
            rblstIHEnm15endstate.SelectedIndex = idx;
            rblstIHEnm15localrequirements.SelectedIndex = idx;
            rblstIHEnm16leadership.SelectedIndex = idx;
            rblstIHEnm16community.SelectedIndex = idx;
            rblstIHEnm16items.SelectedIndex = idx;
            rblstIHEnm17leadership.SelectedIndex = idx;
            rblstIHEnm17partnersplan.SelectedIndex = idx;
            rblstIHEnm17stakeholdersplan.SelectedIndex = idx;
            rblstIHEnm17building.SelectedIndex = idx;
            rblstIHEnm17recordsplan.SelectedIndex = idx;
            rblstIHEnm17organizationsplan.SelectedIndex = idx;
            rblstIHEnm18stakeholdersplan.SelectedIndex = idx;
            rblstIHEnm18parties.SelectedIndex = idx;
            rblstIHEnm18procedures.SelectedIndex = idx;
            rblstIHEnm18needs.SelectedIndex = idx;
            rblstIHEnm18partners.SelectedIndex = idx;
            rblstIHEnm18roles.SelectedIndex = idx;
            rblstIHEnm19Texercises.SelectedIndex = idx;
            rblstIHEnm19Drills.SelectedIndex = idx;
            rblstIHEnm19Fexercises.SelectedIndex = idx;
            rblstIHEnm19FSexercises.SelectedIndex = idx;
            rblstIHEnm20partners.SelectedIndex = idx;
            //rblstIHEnm20IHEOrganization.SelectedIndex = idx;

            rblstIHEnm20concern.SelectedIndex = idx;
            rblstIHEnm20events.SelectedIndex = idx;
            rblstIHEnm20used.SelectedIndex = idx;
            rblstIHEnm20learned.SelectedIndex = idx;
            rblstIHEnm20theplan.SelectedIndex = idx;
            rblstIHEnm21emergencies.SelectedIndex = idx;
            rblstIHEnm21equipment.SelectedIndex = idx;
            rblstIHEnm21standards.SelectedIndex = idx;
            rblstIHEnm21exercises.SelectedIndex = idx;
            rblstIHEnm21community.SelectedIndex = idx;
            rblstIHEnm21hazards.SelectedIndex = idx;
            rblstIHEnm21assessments.SelectedIndex = idx;
            rblstIHEnm22question.SelectedIndex = idx;

        }

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
            ltlpagepos.Text = "1";
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            initialFields(-1);
            Welcome.Attributes.Add("class", "selectedNav");
            if (Session["goback"] == null)
                mpeEOP.Show();
        }

        Session["goback"] = null;
        //if (ltlpagepos.Text == "1")
        //    ddlShcoolType.Visible = true;
        //else
        //    ddlShcoolType.Visible = false;

    }

    protected void K12NextClick(object sender, EventArgs e)
    {
        removeHighlight(mvk12.ActiveViewIndex);
        mvk12.ActiveViewIndex += 1;
        HighlightNav(mvk12.ActiveViewIndex);
        //if (mvk12.ActiveViewIndex > 1)
        ltlpagepos.Text = (Convert.ToInt32(ltlpagepos.Text) + 1).ToString();
    }
    protected void K12PrevClick(object sender, EventArgs e)
    {
        removeHighlight(mvk12.ActiveViewIndex);
        mvk12.ActiveViewIndex -= 1;
        HighlightNav(mvk12.ActiveViewIndex);

        //if(mvk12.ActiveViewIndex>0)
        ltlpagepos.Text = (Convert.ToInt32(ltlpagepos.Text) - 1).ToString();
    }

    protected void IHENextClick(object sender, EventArgs e)
    {
        removeHighlight(mvIHE.ActiveViewIndex);
        mvIHE.ActiveViewIndex += 1;
        HighlightNav(mvIHE.ActiveViewIndex);

       // if (mvIHE.ActiveViewIndex > 1)
       ltlpagepos.Text = (Convert.ToInt32(ltlpagepos.Text) + 1).ToString();

    }

    protected void IHEPrevClick(object sender, EventArgs e)
    {
        removeHighlight(mvIHE.ActiveViewIndex);
        mvIHE.ActiveViewIndex -= 1;
        HighlightNav(mvIHE.ActiveViewIndex);

       // if (mvIHE.ActiveViewIndex > 0)
        ltlpagepos.Text = (Convert.ToInt32(ltlpagepos.Text) - 1).ToString();
    }
    protected void OnEdit(object sender, EventArgs e)
    {

    }

    protected void OnDelete(object sender, EventArgs e)
    {

    }
    protected void btnNew_Click(object sender, EventArgs e)
    {

    }
    protected void ddlShcoolType_SelectedIndexChanged(object sender, EventArgs e)
    {
        initialFields(initIndx);
        if(ddlShcoolType.SelectedValue == "K-12")
        {
            ddlk12orgtype.SelectedIndex = 0;
            ddlk12orgrole.SelectedIndex = 0;
            mvk12.ActiveViewIndex = 0;
            mvk12.Visible = true;
            mvIHE.Visible = false;
        }
        else if (ddlShcoolType.SelectedValue == "IHE")
        {
            ddlIHEorgrole.SelectedIndex = 0;
            ddlIHEorgtype.SelectedIndex = 0;
            mvIHE.ActiveViewIndex = 0;
            mvk12.Visible = false;
            mvIHE.Visible = true;
        }
        else //district EOP
            Response.Redirect("EOPDistrict.aspx");

        if (ltlpagepos.Text == "1")
            ddlShcoolType.Visible = true;
        else
            ddlShcoolType.Visible = false;
    }

    protected void PrintReport(object sender, EventArgs e)
    {
        DataSet sqlDS = getData();

        if(ddlTools.SelectedValue =="1")
            OpenPDF("k12CrystalReport.rpt", sqlDS);
        else if (ddlTools.SelectedValue=="3")
            OpenPDF("IHECrystalReport.rpt", sqlDS);

    }

    private void OpenPDF(string downloadAsFilename, DataSet ds)
    {
        using (ReportDocument Rel = new ReportDocument())
        {
            Rel.Load(Server.MapPath("./reports/" + downloadAsFilename));
            //Rel.SetDatabaseLogon("AppUserREMSContents", "sjfHFt6rghdvejv@3!", "sei-devdb0", "rems");
            Rel.SetDataSource(ds);
            // Stop buffering the response
            Response.Buffer = false;
            // Clear the response content and headers
            Response.ClearContent();
            Response.ClearHeaders();

            ExportFormatType format = ExportFormatType.PortableDocFormat;

            string reportName = "";// downloadAsFilename.Substring(0, downloadAsFilename.Length - 4);
            if (ddlTools.SelectedValue=="1")
                reportName = "REMS K-12 EOP Evaluate Output Report";
            else if (ddlTools.SelectedValue == "3")
                reportName = "REMS IHE EOP Evaluate Output Report";
            try
            {
                Response.Clear();
                Response.AppendCookie(new HttpCookie("fileDownloadToken", hfTokenID.Value)); //downloadTokenValue will have been provided in the form submit via the hidden input field
                Rel.ExportToHttpResponse(format, Response, true, reportName);
            }
            catch (System.Threading.ThreadAbortException)
            {
                //ThreadException can happen for internale Response implementation
            }
            catch (Exception ex)
            {
                //other exeptions will be managed   
                throw;
            }
        }
    }

    private DataSet getData()
    {
        var data = new k12evaltool();
        if (ddlTools.SelectedValue=="1")
        {
            data.schtype = "K-12";
            data.orgrole = ddlk12orgrole.SelectedValue;
            data.orgtype = ddlk12orgtype.SelectedValue;

            data.isschcomleaderAffair = rblstk12isschcomleaderAffair.SelectedIndex;
            data.isschcomadminOffice = rblstk12isschcomadminOffice.SelectedIndex;
            data.isschcomeduDesignee = rblstk12isschcomeduDesignee.SelectedIndex;
            data.isschcomDistrictService = rblstk12isschcomDistrictService.SelectedIndex;
            data.isschcompsychSafety = rblstk12isschcompsychSafety.SelectedIndex;
            data.isschcomconslorOperation = rblstk12isschcomconslorOperation.SelectedIndex;
            data.isschcomnurseService = rblstk12isschcomnurseService.SelectedIndex;
            data.isschcomfmanagerHservice = rblstk12isschcomfmanagerHservice.SelectedIndex;
            data.isschcomtmanagerHRes = rblstk12isschcomtmanagerHRes.SelectedIndex;
            data.isschcomfpersonITech = rblstk12isschcomfpersonITech.SelectedIndex;
            data.isschcomcstaffCousel = rblstk12isschcomcstaffCousel.SelectedIndex;
            data.isschcomSROsOffice = rblstk12isschcomSROsOffice.SelectedIndex;
            data.isschcomITSpecOperation = rblstk12isschcomITSpecOperation.SelectedIndex;
            data.isschcomfsrepLife = rblstk12isschcomfsrepLife.SelectedIndex;
            data.isschcomstudentAffair = rblstk12isschcomstudentAffair.SelectedIndex;
            data.isschcomfamilyTrans = rblstk12isschcomfamilyTrans.SelectedIndex;
            data.ischcomASchrepOffice = rblstk12ischcomASchrepOffice.SelectedIndex;
            data.isINTREPindiv = rblstk12isINTREPindiv.SelectedIndex;
            data.isINTREPracial = rblstk12isINTREPracial.SelectedIndex;
            data.isINTREPReligious = rblstk12isINTREPReligious.SelectedIndex;
            data.isINTREPlimited = rblstk12isINTREPlimited.SelectedIndex;
            data.isComPart1stResp = rblstk12isComPart1stResp.SelectedIndex;
            data.isComPartlocalStaff = rblstk12isComPartlocalStaff.SelectedIndex;
            data.isComPartLawofficer = rblstk12isComPartLawofficer.SelectedIndex;
            data.isComPartEMSPersonnel = rblstk12isComPartEMSPersonnel.SelectedIndex;
            data.isComPartSchResOfficer = rblstk12isComPartSchResOfficer.SelectedIndex;
            data.isComPartFireOfficial = rblstk12isComPartFireOfficial.SelectedIndex;
            data.isComPartPHPract = rblstk12isComPartPHPract.SelectedIndex;
            data.isComPartMHPract = rblstk12isComPartMHPract.SelectedIndex;
            data.isAPRSEMPMLEofficial = rblstk12isAPRSEMPMLEofficial.SelectedIndex;
            data.isAPRSEMCBRorganize = rblstk12isAPRSEMCBRorganize.SelectedIndex;
            data.isAPRSEMBusPart = rblstk12isAPRSEMBusPart.SelectedIndex;
            data.isAPRSEMCBYOrganize = rblstk12isAPRSEMCBYOrganize.SelectedIndex;
            data.isAPRSEMSFLEHSOfficial = rblstk12isAPRSEMSFLEHSOfficial.SelectedIndex;
            data.isAPRSEMDSAGeneral = rblstk12isAPRSEMDSAGeneral.SelectedIndex;
            data.isAPRSEMLSSDepart = rblstk12isAPRSEMLSSDepart.SelectedIndex;
            data.isAPRSEMDisOrganize = rblstk12isAPRSEMDisOrganize.SelectedIndex;
            data.isAPRSEMMedia = rblstk12isAPRSEMMedia.SelectedIndex;
            data.is2NIMS = rblstk12is2NIMS.SelectedIndex;
            data.is2UniRole = rblstk12is2UniRole.SelectedIndex;
            data.is2RegMeeting = rblstk12is2RegMeeting.SelectedIndex;
            data.nm3Highquality = rblstk12nm3Highquality.SelectedIndex;
            data.nm4Highquality = rblstk12nm4Highquality.SelectedIndex;
            data.nm5Highquality = rblstk12nm5Highquality.SelectedIndex;
            data.nm6Highquality = rblstk12nm6Highquality.SelectedIndex;
            data.nm7Highquality = rblstk12nm7Highquality.SelectedIndex;
            data.nm8aHighquality = rblstk12nm8aHighquality.SelectedIndex;
            data.nm8bHighquality = rblstk12nm8bHighquality.SelectedIndex;
            data.nm9Highquality = rblstk12nm9Highquality.SelectedIndex;
            data.is10taken = rblstk12is10taken.SelectedIndex;
            data.is10responsibleaction = rblstk12is10responsibleaction.SelectedIndex;
            data.is10takeplace = rblstk12is10takeplace.SelectedIndex;
            data.is10available = rblstk12is10available.SelectedIndex;
            data.is10prioraction = rblstk12is10prioraction.SelectedIndex;
            data.is10afteraction = rblstk12is10afteraction.SelectedIndex;
            data.is10necessaryaction = rblstk12is10necessaryaction.SelectedIndex;
            data.is10needs = rblstk12is10needs.SelectedIndex;
            data.nm11Highquality = rblstk12nm11Highquality.SelectedIndex;
            data.is12Evacuation = rblstk12is12Evacuation.SelectedIndex;
            data.is12Lockdown = rblstk12is12Lockdown.SelectedIndex;
            data.is12ShelterinPlace = rblstk12is12ShelterinPlace.SelectedIndex;
            data.is12AccountingAllPersons = rblstk12is12AccountingAllPersons.SelectedIndex;
            data.is12ComWarning = rblstk12is12ComWarning.SelectedIndex;
            data.is12ReunificationAssessment = rblstk12is12ReunificationAssessment.SelectedIndex;
            data.is12COOP = rblstk12is12COOP.SelectedIndex;
            data.is12Recovery = rblstk12is12Recovery.SelectedIndex;
            data.is12PHMMHealth = rblstk12is12PHMMHealth.SelectedIndex;
            data.is12Security = rblstk12is12Security.SelectedIndex;
            data.is12onlist = rblstk12is12onlist.SelectedIndex;
            data.is13NHazards = rblstk12is13NHazards.SelectedIndex;
            data.is13THazards = rblstk12is13THazards.SelectedIndex;
            data.is13BHazards = rblstk12is13BHazards.SelectedIndex;
            data.is13Threats = rblstk12is13Threats.SelectedIndex;
            data.nm14flowcharts = rblstk12nm14flowcharts.SelectedIndex;
            data.nm14activevoice = rblstk12nm14activevoice.SelectedIndex;
            data.nm14theyneed = rblstk12nm14theyneed.SelectedIndex;
            data.nm14actionable = rblstk12nm14actionable.SelectedIndex;
            data.nm14audioBraille = rblstk12nm14audioBraille.SelectedIndex;
            data.nm15effectively = rblstk12nm15effectively.SelectedIndex;
            data.nm15assignedfunction = rblstk12nm15assignedfunction.SelectedIndex;
            data.nm15reasonable = rblstk12nm15reasonable.SelectedIndex;
            data.nm15theplan = rblstk12nm15theplan.SelectedIndex;
            data.nm15threathazard = rblstk12nm15threathazard.SelectedIndex;
            data.nm15limitations = rblstk12nm15limitations.SelectedIndex;
            data.nm15thelaw = rblstk12nm15thelaw.SelectedIndex;
            data.nm15idfunctions = rblstk12nm15idfunctions.SelectedIndex;
            data.nm15schcommunity = rblstk12nm15schcommunity.SelectedIndex;
            data.nm15WhoseDirection = rblstk12nm15WhoseDirection.SelectedIndex;
            data.nm15utmostpriority = rblstk12nm15utmostpriority.SelectedIndex;
            data.nm15endstate = rblstk12nm15endstate.SelectedIndex;
            data.nm15localrequirements = rblstk12nm15localrequirements.SelectedIndex;
            data.nm16leadership = rblstk12nm16leadership.SelectedIndex;
            data.nm16community = rblstk12nm16community.SelectedIndex;
            data.nm16items = rblstk12nm16items.SelectedIndex;
            data.nm17leadership = rblstk12nm17leadership.SelectedIndex;
            data.nm17partnersplan = rblstk12nm17partnersplan.SelectedIndex;
            data.nm17stakeholdersplan = rblstk12nm17stakeholdersplan.SelectedIndex;
            data.nm17building = rblstk12nm17building.SelectedIndex;
            data.nm17recordsplan = rblstk12nm17recordsplan.SelectedIndex;
            data.nm17organizationsplan = rblstk12nm17organizationsplan.SelectedIndex;
            data.nm18stakeholdersplan = rblstk12nm18stakeholdersplan.SelectedIndex;
            data.nm18parties = rblstk12nm18parties.SelectedIndex;
            data.nm18procedures = rblstk12nm18procedures.SelectedIndex;
            data.nm18needs = rblstk12nm18needs.SelectedIndex;
            data.nm18partners = rblstk12nm18partners.SelectedIndex;
            data.nm18roles = rblstk12nm18roles.SelectedIndex;
            data.nm19Texercises = rblstk12nm19Texercises.SelectedIndex;
            data.nm19Drills = rblstk12nm19Drills.SelectedIndex;
            data.nm19Fexercises = rblstk12nm19Fexercises.SelectedIndex;
            data.nm19FSexercises = rblstk12nm19FSexercises.SelectedIndex;
            data.nm20partners = rblstk12nm20partners.SelectedIndex;
            data.nm20concern = rblstk12nm20concern.SelectedIndex;
            data.nm20events = rblstk12nm20events.SelectedIndex;
            data.nm20used = rblstk12nm20used.SelectedIndex;
            data.nm20learned = rblstk12nm20learned.SelectedIndex;
            data.nm20theplan = rblstk12nm20theplan.SelectedIndex;
            data.nm21emergencies = rblstk12nm21emergencies.SelectedIndex;
            data.nm21equipment = rblstk12nm21equipment.SelectedIndex;
            data.nm21standards = rblstk12nm21standards.SelectedIndex;
            data.nm21exercises = rblstk12nm21exercises.SelectedIndex;
            data.nm21community = rblstk12nm21community.SelectedIndex;
            data.nm21hazards = rblstk12nm21hazards.SelectedIndex;
            data.nm21assessments = rblstk12nm21assessments.SelectedIndex;
            data.nm22question = rblstk12nm22question.SelectedIndex;
        }
        else if (ddlTools.SelectedValue == "3")
        {
            data.schtype = "IHE";
            data.orgrole = ddlk12orgrole.SelectedValue;
            data.orgtype = ddlk12orgtype.SelectedValue;

            data.isschcomleaderAffair = rblstIHEisschcomleaderAffair.SelectedIndex;
            data.isschcomadminOffice = rblstIHEisschcomadminOffice.SelectedIndex;
            data.isschcomeduDesignee = rblstIHEisschcomeduDesignee.SelectedIndex;
            data.isschcomDistrictService = rblstIHEisschcomDistrictService.SelectedIndex;
            data.isschcompsychSafety = rblstIHEisschcompsychSafety.SelectedIndex;
            data.isschcomconslorOperation = rblstIHEisschcomconslorOperation.SelectedIndex;
            data.isschcomnurseService = rblstIHEisschcomnurseService.SelectedIndex;
            data.isschcomfmanagerHservice = rblstIHEisschcomfmanagerHservice.SelectedIndex;
            data.isschcomtmanagerHRes = rblstIHEisschcomtmanagerHRes.SelectedIndex;
            data.isschcomfpersonITech = rblstIHEisschcomfpersonITech.SelectedIndex;
            data.isschcomcstaffCousel = rblstIHEisschcomcstaffCousel.SelectedIndex;
            data.isschcomSROsOffice = rblstIHEisschcomSROsOffice.SelectedIndex;
            data.isschcomITSpecOperation = rblstIHEisschcomITSpecOperation.SelectedIndex;
            data.isschcomfsrepLife = rblstIHEisschcomfsrepLife.SelectedIndex;
            data.isschcomstudentAffair = rblstIHEisschcomstudentAffair.SelectedIndex;
            data.isschcomfamilyTrans = rblstIHEisschcomfamilyTrans.SelectedIndex;
            data.ischcomASchrepOffice = rblstIHEischcomASchrepOffice.SelectedIndex;
            data.isschcomIHErep = rblstIHEisschcomIHErep.SelectedIndex;

            data.isINTREPindiv = rblstIHEisINTREPindiv.SelectedIndex;
            data.isINTREPracial = rblstIHEisINTREPracial.SelectedIndex;
            data.isINTREPReligious = rblstIHEisINTREPReligious.SelectedIndex;
            data.isINTREPiheStudents = rblstIHEisINTREPiheStudents.SelectedIndex;

            data.isINTREPlimited = rblstIHEisINTREPlimited.SelectedIndex;
            data.isComPart1stResp = rblstIHEisComPart1stResp.SelectedIndex;
            data.isComPartlocalStaff = rblstIHEisComPartlocalStaff.SelectedIndex;
            data.isComPartLawofficer = rblstIHEisComPartLawofficer.SelectedIndex;
            data.isComPartEMSPersonnel = rblstIHEisComPartEMSPersonnel.SelectedIndex;
            //data.isComPartSchResOfficer = rblstIHEisComPartSchResOfficer.SelectedIndex;
            data.isComPartFireOfficial = rblstIHEisComPartFireOfficial.SelectedIndex;
            data.isComPartPHPract = rblstIHEisComPartPHPract.SelectedIndex;
            data.isComPartMHPract = rblstIHEisComPartMHPract.SelectedIndex;
            data.isAPRSEMPMLEofficial = rblstIHEisAPRSEMPMLEofficial.SelectedIndex;
            data.isAPRSEMCBRorganize = rblstIHEisAPRSEMCBRorganize.SelectedIndex;
            data.isAPRSEMBusPart = rblstIHEisAPRSEMBusPart.SelectedIndex;
            data.isAPRSEMCBYOrganize = rblstIHEisAPRSEMCBYOrganize.SelectedIndex;
            data.isAPRSEMSFLEHSOfficial = rblstIHEisAPRSEMSFLEHSOfficial.SelectedIndex;
            data.isAPRSEMDSAGeneral = rblstIHEisAPRSEMDSAGeneral.SelectedIndex;
            data.isAPRSEMLSSDepart = rblstIHEisAPRSEMLSSDepart.SelectedIndex;
            data.isAPRSEMDisOrganize = rblstIHEisAPRSEMDisOrganize.SelectedIndex;
            data.isAPRSEMMedia = rblstIHEisAPRSEMMedia.SelectedIndex;
            data.is2NIMS = rblstIHEis2NIMS.SelectedIndex;
            data.is2UniRole = rblstIHEis2UniRole.SelectedIndex;
            data.is2RegMeeting = rblstIHEis2RegMeeting.SelectedIndex;
            data.nm3Highquality = rblstIHEnm3Highquality.SelectedIndex;
            data.nm4Highquality = rblstIHEnm4Highquality.SelectedIndex;
            data.nm5Highquality = rblstIHEnm5Highquality.SelectedIndex;
            data.nm6Highquality = rblstIHEnm6Highquality.SelectedIndex;
            data.nm7Highquality = rblstIHEnm7Highquality.SelectedIndex;
            data.nm8aHighquality = rblstIHEnm8aHighquality.SelectedIndex;
            data.nm8bHighquality = rblstIHEnm8bHighquality.SelectedIndex;
            data.nm9Highquality = rblstIHEnm9Highquality.SelectedIndex;
            data.is10taken = rblstIHEis10taken.SelectedIndex;
            data.is10responsibleaction = rblstIHEis10responsibleaction.SelectedIndex;
            data.is10takeplace = rblstIHEis10takeplace.SelectedIndex;
            data.is10available = rblstIHEis10available.SelectedIndex;
            data.is10prioraction = rblstIHEis10prioraction.SelectedIndex;
            data.is10afteraction = rblstIHEis10afteraction.SelectedIndex;
            data.is10necessaryaction = rblstIHEis10necessaryaction.SelectedIndex;
            data.is10needs = rblstIHEis10needs.SelectedIndex;
            data.nm11Highquality = rblstIHEnm11Highquality.SelectedIndex;
            data.is12Evacuation = rblstIHEis12Evacuation.SelectedIndex;
            data.is12Lockdown = rblstIHEis12Lockdown.SelectedIndex;
            data.is12ShelterinPlace = rblstIHEis12ShelterinPlace.SelectedIndex;
            data.is12AccountingAllPersons = rblstIHEis12AccountingAllPersons.SelectedIndex;
            data.is12ComWarning = rblstIHEis12ComWarning.SelectedIndex;
            data.is12ReunificationAssessment = rblstIHEis12ReunificationAssessment.SelectedIndex;
            data.is12COOP = rblstIHEis12COOP.SelectedIndex;
            data.is12Recovery = rblstIHEis12Recovery.SelectedIndex;
            data.is12PHMMHealth = rblstIHEis12PHMMHealth.SelectedIndex;
            data.is12Security = rblstIHEis12Security.SelectedIndex;
            data.is12onlist = rblstIHEis12onlist.SelectedIndex;
            data.is13NHazards = rblstIHEis13NHazards.SelectedIndex;
            data.is13THazards = rblstIHEis13THazards.SelectedIndex;
            //data.is13BHazards = rblstIHEis13BHazards.SelectedIndex;
            data.is13Threats = rblstIHEis13Threats.SelectedIndex;
            data.nm14flowcharts = rblstIHEnm14flowcharts.SelectedIndex;
            data.nm14activevoice = rblstIHEnm14activevoice.SelectedIndex;
            data.nm14theyneed = rblstIHEnm14theyneed.SelectedIndex;
            data.nm14actionable = rblstIHEnm14actionable.SelectedIndex;
            data.nm14audioBraille = rblstIHEnm14audioBraille.SelectedIndex;
            data.nm15effectively = rblstIHEnm15effectively.SelectedIndex;
            data.nm15assignedfunction = rblstIHEnm15assignedfunction.SelectedIndex;
            data.nm15reasonable = rblstIHEnm15reasonable.SelectedIndex;
            data.nm15theplan = rblstIHEnm15theplan.SelectedIndex;
            data.nm15threathazard = rblstIHEnm15threathazard.SelectedIndex;
            data.nm15limitations = rblstIHEnm15limitations.SelectedIndex;
            data.nm15thelaw = rblstIHEnm15thelaw.SelectedIndex;
            data.nm15idfunctions = rblstIHEnm15idfunctions.SelectedIndex;
            data.nm15schcommunity = rblstIHEnm15schcommunity.SelectedIndex;
            data.nm15WhoseDirection = rblstIHEnm15WhoseDirection.SelectedIndex;
            data.nm15utmostpriority = rblstIHEnm15utmostpriority.SelectedIndex;
            data.nm15endstate = rblstIHEnm15endstate.SelectedIndex;
            data.nm15localrequirements = rblstIHEnm15localrequirements.SelectedIndex;
            data.nm16leadership = rblstIHEnm16leadership.SelectedIndex;
            data.nm16community = rblstIHEnm16community.SelectedIndex;
            data.nm16items = rblstIHEnm16items.SelectedIndex;
            data.nm17leadership = rblstIHEnm17leadership.SelectedIndex;
            data.nm17partnersplan = rblstIHEnm17partnersplan.SelectedIndex;
            data.nm17stakeholdersplan = rblstIHEnm17stakeholdersplan.SelectedIndex;
            data.nm17building = rblstIHEnm17building.SelectedIndex;
            data.nm17recordsplan = rblstIHEnm17recordsplan.SelectedIndex;
            data.nm17organizationsplan = rblstIHEnm17organizationsplan.SelectedIndex;
            data.nm18stakeholdersplan = rblstIHEnm18stakeholdersplan.SelectedIndex;
            data.nm18parties = rblstIHEnm18parties.SelectedIndex;
            data.nm18procedures = rblstIHEnm18procedures.SelectedIndex;
            data.nm18needs = rblstIHEnm18needs.SelectedIndex;
            data.nm18partners = rblstIHEnm18partners.SelectedIndex;
            data.nm18roles = rblstIHEnm18roles.SelectedIndex;
            data.nm19Texercises = rblstIHEnm19Texercises.SelectedIndex;
            data.nm19Drills = rblstIHEnm19Drills.SelectedIndex;
            data.nm19Fexercises = rblstIHEnm19Fexercises.SelectedIndex;
            data.nm19FSexercises = rblstIHEnm19FSexercises.SelectedIndex;
            data.nm20partners = rblstIHEnm20partners.SelectedIndex;
            //data.nm20IHEOrganization = rblstIHEnm20IHEOrganization.SelectedIndex;

            data.nm20concern = rblstIHEnm20concern.SelectedIndex;
            data.nm20events = rblstIHEnm20events.SelectedIndex;
            data.nm20used = rblstIHEnm20used.SelectedIndex;
            data.nm20learned = rblstIHEnm20learned.SelectedIndex;
            data.nm20theplan = rblstIHEnm20theplan.SelectedIndex;
            data.nm21emergencies = rblstIHEnm21emergencies.SelectedIndex;
            data.nm21equipment = rblstIHEnm21equipment.SelectedIndex;
            data.nm21standards = rblstIHEnm21standards.SelectedIndex;
            data.nm21exercises = rblstIHEnm21exercises.SelectedIndex;
            data.nm21community = rblstIHEnm21community.SelectedIndex;
            data.nm21hazards = rblstIHEnm21hazards.SelectedIndex;
            data.nm21assessments = rblstIHEnm21assessments.SelectedIndex;
            data.nm22question = rblstIHEnm22question.SelectedIndex;

        }

        data.createdon = DateTime.Now;
        data.modifiedon = DateTime.Now;
        db.k12evaltools.InsertOnSubmit(data);
        try
        {
            db.SubmitChanges();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            // Make some adjustments. 
            // ... 
            // Try again.
            db.SubmitChanges();
        }

        DataTable rcdItem = (from rpt in db.k12evaltools
                       where rpt.id == data.id
                       select rpt).ToDataTable();

        DataSet dsForm = new DataSet();

            rcdItem.TableName = "k12evaltool";


        dsForm.Tables.Add(rcdItem);
        return dsForm;
    }

    private void removeHighlight(int index)
    {
        switch (index)
        {
            case 0:
                Welcome.Attributes.Remove("class");
                break;
            case 1:
                Step1.Attributes.Remove("class");
                break;
            case 2:
                Step2.Attributes.Remove("class");
                break;
            case 3:
                Step3.Attributes.Remove("class");
                break;
            case 4:
                Step4.Attributes.Remove("class");
                break;
            case 5:
                Step5.Attributes.Remove("class");
                break;
            case 6:
                Step6.Attributes.Remove("class");
                break;
            case 7:
                Conclusion.Attributes.Remove("class");
                break;
            default:
                Welcome.Attributes.Remove("class");
                break;
        }
    }

    private void HighlightNav(int index)
    {
        switch (index)
        {
            case 0:
                Welcome.Attributes.Add("class", "selectedNav");
                break;
            case 1:
                Step1.Attributes.Add("class", "selectedNav");
                break;
            case 2:
                Step2.Attributes.Add("class", "selectedNav");
                break;
            case 3:
                Step3.Attributes.Add("class", "selectedNav");
                break;
            case 4:
                Step4.Attributes.Add("class", "selectedNav");
                break;
            case 5:
                Step5.Attributes.Add("class", "selectedNav");
                break;
            case 6:
                Step6.Attributes.Add("class", "selectedNav");
                break;
            case 7:
                Conclusion.Attributes.Add("class", "selectedNav");
                break;
            default:
                Welcome.Attributes.Add("class", "selectedNav");
                break;
        }
    }

    protected void OnSelectTool(object sender, EventArgs e)
    {
        switch (ddlTools.SelectedIndex)
        {
            case 0:
                rfvpopup.IsValid = false;
                mpeEOP.Show();
                break;
            case 1:
            ddlk12orgtype.SelectedIndex = 0;
            ddlk12orgrole.SelectedIndex = 0;
            mvk12.ActiveViewIndex = 0;
            mvk12.Visible = true;
            mvIHE.Visible = false;
                break;
            case 2:
            ddlIHEorgrole.SelectedIndex = 0;
            ddlIHEorgtype.SelectedIndex = 0;
            mvIHE.ActiveViewIndex = 0;
            mvk12.Visible = false;
            mvIHE.Visible = true;
                break;
        }
        //mpeEOP.Hide();
    }

    protected void ClosePan(object sender, EventArgs e)
    {
        mpeEOP.Hide();
    }

    protected void ddlToolSelected(object sender, EventArgs e)
    {
        initialFields(initIndx);
    }
}