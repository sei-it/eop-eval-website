﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EOPDistrict : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Welcome.Attributes.Add("class", "selectedNav");
            ltlPageNo.Text = "Page 1 of 6";
        }
    }
    protected void NextClick(object sender, EventArgs e)
    {
        if (DistrictEOP.ActiveViewIndex > 6)
            return;

        removeHighlight(DistrictEOP.ActiveViewIndex);
        if (rbl311.SelectedValue == "No" && DistrictEOP.ActiveViewIndex == 2)
        {
            DistrictEOP.ActiveViewIndex += 4;
        }
        else
            DistrictEOP.ActiveViewIndex += 1;

        HighlightNav(DistrictEOP.ActiveViewIndex);
    }

   
    protected void PrevClick(object sender, EventArgs e)
    {
        if (DistrictEOP.ActiveViewIndex > 6)
            return;

        removeHighlight(DistrictEOP.ActiveViewIndex);
        if (rbl311.SelectedValue == "No" && DistrictEOP.ActiveViewIndex==6)
        {
            DistrictEOP.ActiveViewIndex -= 4;
        }
        else
            DistrictEOP.ActiveViewIndex -= 1;

        HighlightNav(DistrictEOP.ActiveViewIndex);
    }

    private void removeHighlight(int index)
    {
        switch (index)
        {
            case 0:
                Welcome.Attributes.Remove("class");
                break;
            case 1:
                Welcome.Attributes.Remove("class");
                break;
            case 2:
                plan.Attributes.Remove("class");
                break;
            case 3:
                emergency.Attributes.Remove("class");
                break;
            case 4:
                wide_emergencies.Attributes.Remove("class");
                break;
            case 5:
                school.Attributes.Remove("class");
                break;
            case 6:
                Conclusion.Attributes.Remove("class");
                break;
            default:
                Welcome.Attributes.Remove("class");
                break;
        }
    }

    private void HighlightNav(int index)
    {
        switch (index)
        {
            case 0:
                Welcome.Attributes.Add("class", "selectedNav");
                ltlPageNo.Text = rbl311.SelectedValue == "No" ? "Page 1 of 3" : "Page 1 of 6";
                break;
            case 1:
                Welcome.Attributes.Add("class", "selectedNav");
                ltlPageNo.Text = rbl311.SelectedValue == "No" ? "Page 1 of 3" : "Page 1 of 6";
                break;
            case 2:
                plan.Attributes.Add("class", "selectedNav");
                ltlPageNo.Text = rbl311.SelectedValue == "No" ? "Page 2 of 3" : "Page 2 of 6";
                break;
            case 3:
                emergency.Attributes.Add("class", "selectedNav");
                ltlPageNo.Text = "Page 3 of 6";
                break;
            case 4:
                wide_emergencies.Attributes.Add("class", "selectedNav");
                ltlPageNo.Text = "Page 4 of 6";
                break;
            case 5:
                school.Attributes.Add("class", "selectedNav");
                ltlPageNo.Text = "Page 5 of 6";
                break;
            case 6:
                Conclusion.Attributes.Add("class", "selectedNav");
                ltlPageNo.Text = rbl311.SelectedValue == "No" ? "Page 3 of 3" : "Page 6 of 6";
                break;
            default:
                Welcome.Attributes.Add("class", "selectedNav");
                ltlPageNo.Text =  "Page 1 of 6";
                break;
        }
    }

    protected void rbl311_SelectedIndexChanged(object sender, EventArgs e)  //District plan
    {
        if (rbl311.SelectedValue == "No")
        {
            DistrictplanShowHide(false);
            ltlPageNo.Text = "Page 2 of 3";
        }
        else //Yes
        {
            DistrictplanShowHide(true);
            ltlPageNo.Text = "Page 2 of 6";
        }
    }

        private void DistrictplanShowHide(bool bstatus)
        {
            districtplan1.Visible = bstatus;
            districtplan2.Visible = bstatus;
            districtplan3.Visible = bstatus;
            districtplan4.Visible = bstatus;
            districtplan5.Visible = bstatus;
            districtplan6.Visible = bstatus;
            districtplan7.Visible = bstatus;

            nav3.Visible = bstatus;
            nav4.Visible = bstatus;
            nav5.Visible = bstatus;

            setValidationControls(bstatus);

        }

        private void setValidationControls(bool bVal)
        {
            for (int i=1; i<14; i++)
            {
                RequiredFieldValidator rqval = DistrictEOP.FindControl("View3").FindControl("RequiredFieldValidator"+i) as RequiredFieldValidator;
                rqval.Enabled = bVal;

            }
        }


        protected void rbl6121_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbl6121.SelectedValue == "No")
            {
                schoolplan1.Visible = false;
                schoolplan2.Visible = false;
                schoolplan3.Visible = false;
                schoolplan4.Visible = false;
                schoolplan5.Visible = false;
                schoolplan6.Visible = false;
                schoolplan7.Visible = false;
                schoolplan8.Visible = false;
                schoolplan9.Visible = false;
                setSchoolPlanValidationControls(false);
            }
            else
            {
                schoolplan1.Visible = true;
                schoolplan2.Visible = true;
                schoolplan3.Visible = true;
                schoolplan4.Visible = true;
                schoolplan5.Visible = true;
                schoolplan6.Visible = true;
                schoolplan7.Visible = true;
                schoolplan8.Visible = true;
                schoolplan9.Visible = true;
                setSchoolPlanValidationControls(true);
            }
        }

        private void setSchoolPlanValidationControls(bool bVal)
        {
            for (int i = 33; i < 42; i++)
            {
                RequiredFieldValidator rqval = DistrictEOP.FindControl("View6").FindControl("RequiredFieldValidator" + i) as RequiredFieldValidator;
                rqval.Enabled = bVal;
            }
        }

        protected void PrintReport(object sender, EventArgs e)
        {

        }

        protected void GoHome(object sender, EventArgs e)
        {
            Session["goback"] = true;
            Response.Redirect("k12IHEevalTool.aspx");
        }
}